// pages/order_notice/order_notice.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        orderTitle: '商品订单',
        // 消息源数据
        messList: [],
        checked: false,
        // 模拟分页操作相关参数
        showMessList: [],    // 展示在页面的数据列表
        pageNum: 1,          // 当前页码
        pageSize: 6,         // 页量
        total: 0,            // 消息总数
        shopStateText: ['待支付', '待发货', '已发货', '售后中', '已完成', '待退款', '已退款']
    },
    /**
     * 将消息设为已读
     */
    readMess() {
        if(this.data.orderTitle === '回收订单' && !this.data.checked) {
            wx.cloud.callFunction({
                name: 'databaseApi',
                data: {
                    type: 'readMess',
                    reqData: 'recycle'
                }
            }).then(res => {
                // console.log('readRecycleMess ==>', res);
            }, err => {
                console.log('readMess ==>', err);
            });
        }else if(this.data.orderTitle === '商品订单' && !this.data.checked) {
            wx.cloud.callFunction({
                name: 'databaseApi',
                data: {
                    type: 'readMess',
                    reqData: 'shop'
                }
            }).then(res => {
                // console.log('readGoodsMess ==>', res);
            }, err => {
                console.log('readMess ==>', err);
            });
        }
        
    },
    /**
     * 获取订单信息列表
     */
    getMessList() {
        if(this.data.orderTitle === '回收订单') {
            this.getOriginMess('recycle');
        }else {
            this.getOriginMess('shop');
        }
    },
    /**
     * 获取原始消息
     * @param {*} type 
     */
    getOriginMess(type) {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getMessList',
                reqData: {
                    timeStamp: new Date().getTime(),
                    type: type
                }
            }
        }).then(res => {
            // console.log('getRecycleMessList success ==>', res);
            this.setData({
                messList: [...this.data.messList, ...res.result.resData],
                total: res.result.resData.length
            });
            this.getShowMess(1);
        }, err => {
            console.log('getMessList error ==>', err);
        });
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if(options) {
            this.setData({
                orderTitle: options.type,
                checked: JSON.parse(options.checked)
            }, () => {
                this.getMessList();
            });
        }
        wx.setNavigationBarTitle({
          title: this.data.orderTitle + '消息',
        });
        
    },
    /**
     * 获取展示消息
     * @param {*} pageNum 
     */
    getShowMess(pageNum) {
        wx.showLoading({
          title: '加载中',
        });
        // 每次从源数据中切一页赋给展示列表
        let begin = (pageNum - 1) * this.data.pageSize;
        let end = pageNum * this.data.pageSize;
        setTimeout(() => {
            this.setData({
                ['showMessList[' + (pageNum - 1) + ']']: this.data.messList.slice(begin, end)
            }, () => {
                wx.hideLoading();
            });
        }, 500);
        
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        // this.getShowMess(1);
        this.readMess();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {
        // console.log('hide');
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
        // console.log('unload');
        this.readMess();
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
        this.setData({
            showMessList: [],
            pageNum: 1
        });
        this.getShowMess(this.data.pageNum);
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        if(this.data.pageNum * this.data.pageSize < this.data.total) {
            this.setData({
                pageNum: this.data.pageNum + 1
            });
            this.getShowMess(this.data.pageNum);
        }else {
            this.$showMsg('没有更多数据了');
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    /**
     * 弹出提示消息
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    }
})