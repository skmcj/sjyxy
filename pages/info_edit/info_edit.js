// pages/info_edit/info_edit.js
const app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        type: 'sex',
        name: '',
        sex: 0,
        userInfo: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if(options.type) {
            this.setData({
                type: options.type
            });
            if(options.type === 'nick') {
                this.setData({
                    name: options.name
                });
            }else {
                this.setData({
                    sex: JSON.parse(options.sex)
                });
            }
        }
    },
    /**
     * 监听昵称输入
     * @param {*} e 
     */
    changeNickName(e) {
        let name = e.detail.value;
        // console.log('name ==>', name);
        this.setData({
            name: name
        });
    },
    /**
     * 监听性别选择
     * @param {*} e 
     */
    onSelectSex(e) {
        // console.log('e ==>', e.currentTarget.dataset.sex);
        let sex = e.currentTarget.dataset.sex;
        this.setData({
            sex: sex
        });
    },
    /**
     * 点击完成按钮
     */
    onComplete() {
        if(this.data.type === 'nick') {
            // console.log('name ==>', this.data.name);
            this.updateNickName();
        }else {
            // console.log('sex ==>', this.data.sex);
            this.updateSex();
        }
    },
    /**
     * 修改昵称
     */
    updateNickName() {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'updateUserInfo',
                reqData: {
                    type: 'nickName',
                    value: this.data.name
                }
            }
        }).then(res => {
            console.log('update nickname success ==>', res);
            // 更新TIM的数据
            wx.$TUIKit.updateMyProfile({
                nick: this.data.name
            });
            // 存储修改
            this.setData({
                ['userInfo.nickName']: this.data.name
            });
            app.globalData.userInfo = this.data.userInfo;
            wx.setStorageSync('userInfo', this.data.userInfo);
            this.$showMsg('修改成功', 'success');
            // 800ms后跳转到详情页
            setTimeout(() => {
                wx.redirectTo({
                    url: '/pages/personal_info/personal_info',
                });
            }, 800);
        }, err => {
            console.log('update nickname error ==>', err);
            this.$showMsg('系统繁忙，请稍后重试');
        });
    },
    /**
     * 修改性别
     */
    updateSex() {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'updateUserInfo',
                reqData: {
                    type: 'gender',
                    value: this.data.sex
                }
            }
        }).then(res => {
            console.log('update sex success ==>', res);
            // 更新TIM的数据
            wx.$TUIKit.updateMyProfile({
                gender: this.data.sex === 1 ? wx.$TUIKitTIM.TYPES.GENDER_FEMALE : wx.$TUIKitTIM.TYPES.GENDER_MALE
            });
            // 存储修改
            this.setData({
                ['userInfo.gender']: this.data.sex
            });
            app.globalData.userInfo = this.data.userInfo;
            wx.setStorageSync('userInfo', this.data.userInfo);
            this.$showMsg('修改成功', 'success');
            // 800ms后跳转到详情页
            setTimeout(() => {
                wx.redirectTo({
                    url: '/pages/personal_info/personal_info',
                });
            }, 800);
        }, err => {
            console.log('update sex error ==>', err);
            this.$showMsg('系统繁忙，请稍后重试');
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getUserInfo();
    },
    /**
     * 获取登录用户信息
     */
    getUserInfo() {
        let userInfo = wx.getStorageSync('userInfo');
        if(userInfo) {
            this.setData({
                userInfo: userInfo,
            });
        }
        // console.log('userInfo ==>', this.data.userInfo);
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
            title: msg,
            icon: type
        });
    }
})