// pages/order_details/order_details.js
const db = wx.cloud.database();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        orderId: '',
        orderForm: '',
        orderType: 1,
        // 订单状态
        orderStatus: ['待付款', '待发货', '已发货', '售后中', '已完成', '待退款', '已退款']
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if(options) {
            this.setData({
                orderType: JSON.parse(options.type),
                orderId: options.id
            });
        }
    },
    /**
     * 联系卖家
     */
    onToContactSeller() {
        let userID = this.data.orderForm.goodsMess.userID;
        const payloadData = {
            conversationID: `C2C${userID}`,
        };
        wx.navigateTo({
            url: `/pages/chat/chat?conversationInfomation=${JSON.stringify(payloadData)}`,
        });
    },
    /**
     * 联系买家
     */
    onToContactBuyer() {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getBuyIdByOrderId',
                reqData: this.data.orderForm._id
            }
        }).then(res => {
            console.log('get buyer userID success ==>', res);
            let userID = res.result.resData;
            const payloadData = {
                conversationID: `C2C${userID}`,
            };
            wx.navigateTo({
                url: `/pages/chat/chat?conversationInfomation=${JSON.stringify(payloadData)}`,
            });
        }, err => {
            console.log('get buyer uerID error ==>', err);
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getOrderData();
    },
    /**
     * 获取订单数据
     */
    getOrderData() {
        if(this.data.orderType === 1) {
            this.getRecycleOrder();
        }else {
            this.getGoodsOrder();
        }
    },
    /**
     * 获取回收订单数据
     */
    getRecycleOrder() {
        // 根据 id 获取订单信息
        db.collection('recycle_order').doc(this.data.orderId).get()
          .then(res => {
            //   console.log('get order by id success ==>', res);
              this.setData({
                  orderForm: res.data.orderForm
              });
          }, err => {
              console.log('get order by id error ==>', err);
          });
    },
    /**
     * 获取商品订单数据
     */
    getGoodsOrder() {
        db.collection('idlemallOrder').doc(this.data.orderId).get()
          .then(res => {
              if(res.data.createTime) {
                  res.data.createTime = this.getTimeFormat(res.data.createTime);
              }
              if(res.data.payTime) {
                  res.data.payTime = this.getTimeFormat(res.data.payTime);
              }
              // console.log('get order by id success ==>', res);
              this.setData({
                  orderForm: res.data
              });
          }, err => {
              console.log('get order by id error ==>', err);
          });
    },
    /**
     * 格式化时间
     * @param {*} dateStr 
     */
    getTimeFormat(dateStr) {
        let date = new Date(dateStr);
        let fDateStr = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        let fTimeStr = ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
        console.log('dateStr ==>', dateStr, fDateStr + ' ' + fTimeStr);
        return fDateStr + ' ' + fTimeStr;
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})