// pages/New_Address/New_Address.js
Page({
    /**
     * 页面的初始数据
     */
    data: {
        address: {
          name: "",
          sex: "先生",
          phone: "",
          location: "",
          desc: "",
          default: false
        },
        btnText: '保存',
        isEdit: false
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {},
    /**
     * 监听用户名输入
     * @param {Object} e 
     */
    changeName(e) {
      this.setData({
        'address.name': e.detail.value
      });
    },
    /**
     * 监听用户性别选择
     * @param {Object} e 
     */
    changeSex(e) {
      let sexStr = '';
      if(e.detail.value === 'boy') {
        sexStr = '先生';
      }else {
        sexStr = '女士';
      }
      this.setData({
        'address.sex': sexStr
      });
    },
    /**
     * 监听用户电话输入
     * @param {Object} e 
     */
    changePhone(e) {
      this.setData({
        'address.phone': e.detail.value
      });
    },
    /**
     * 监听用户定位信息输入
     */
    changeLocation() {
      wx.chooseLocation()
        .then(res => {
          this.setData({
            'address.location': res.address
          });
        }, err => {
          console.log('Location err ==> ', err);
        });
    },
    /**
     * 监听用户详细地址输入
     * @param {Object} e 
     */
    changeDesc(e) {
      this.setData({
        'address.desc': e.detail.value
      });
    },
    /**
     * 保存输入地址
     */
    onSaveAddress() {
      // console.log('address ==> ', this.data.address);
      // const db = wx.cloud.database();
      // 验证格式 | 信息完整
      if(this.checkAddressFormat()) {
          if (!this.data.isEdit) {
            // 保存地址信息到数据库
            wx.cloud.callFunction({
              name: 'databaseApi',
              data: {
                type: 'saveAddress',
                reqData: this.data.address
              }
            }).then(res => {
              // 地址保存成功
              // console.log('Location save success ==> ', res);
              // 跳转回地址管理界面
              return wx.redirectTo({
                url: '/pages/address_administration/address_administration',
              });
            }, err => {
              // 地址保存失败
              console.log('Location save fail ==> ', err);
            }).then(res => {
              // 跳转成功
              console.log('NavToAddressManage res ==> ', res);
            }, err => {
              // 跳转失败
              console.log('NavToAddressManage err ==> ', err);
            });
          } else {
            // 保存更改到本地
            wx.setStorageSync('editAddress', this.data.address);
            // 跳转页面
            wx.redirectTo({
              url: '/pages/address_administration/address_administration',
            });
          }
      }
    },
    /**
     * 检查是否为修改地址
     */
    checkEditAddress() {
      let editIndex = wx.getStorageSync('editIndex');
      let editAddress = wx.getStorageSync('editAddress');
      // console.log('editIndex ==>', editIndex);
      // console.log('editAddress ==>', editAddress);
      if(editIndex !== '') {
        this.setData({
          address: editAddress,
          btnText: '更改',
          isEdit: true
        });
        wx.setNavigationBarTitle({
          title: '编辑地址',
        });
      }
    },
    /**
     * 验证address的格式是否合理
     */
    checkAddressFormat() {
      let flag = true;
      if(this.data.address.location === '') {
        flag = false;
        this.showMsg('请先点击获取定位信息', 'none');
      }else if(this.data.address.name === '') {
        flag = false;
        this.showMsg('请输入联系人姓名', 'none');
      }else if(!this.checkPhone(this.data.address.phone)) {
        flag = false;
        this.showMsg('请输入正确的手机号码', 'none');
      }
      return flag;
    },
    /**
     * 检查手机号码格式
     * @param {String} phoneNum 手机号码
     */
    checkPhone(phoneNum) {
      const phoneReg = /^1[3-9]\d{9}$/;
      return phoneReg.test(phoneNum);
    },
    /**
     * 显示消息提示
     * @param {String} msg 消息体
     * @param {String} type 提示类型[loading|success|error|none]
     */
    showMsg(msg, type) {
      wx.showToast({
        title: msg,
        icon: type
      });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
      this.checkEditAddress();
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})