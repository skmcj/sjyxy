// pages/denglu/denglu.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        isShow: false,             // 是否显示协议
        isAgree: false,            // 是否同意协议
        // fromPage: '/pages/my/my'   // 上一级页面
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // 如果有传参
        // if(options) {
        //     // 如果传参中存在fromPage这一属性
        //     if(options.fromPage) {
        //         this.setData({
        //             fromPage: options.fromPage
        //         }); 
        //     }
        // }
    },
    /**
     * 登录事件
     */
    onLogin() {
        if (!this.data.isAgree) {
            wx.showToast({
                title: '请阅读并勾选页面底部协议',
                icon: 'none'
            });
        } else {
            wx.getUserProfile({
                desc: '便于您使用我们的产品与服务',
            }).then(res => {
                // 登录成功，查询数据库有无记录，判断是否新用户
                return wx.cloud.callFunction({
                    name: 'databaseApi',
                    data: {
                        type: 'login',
                        reqData: res.userInfo
                    }
                });
            }, err => {
                // 登录失败 | 拒绝授权
                console.log('Login err ~ ', err);
            }).then(res => {
                // 登录成功，将数据存储到本地
                app.globalData.userInfo = res.result.resData.userInfo;
                app.globalData.userID = res.result.resData.userID;
                app.globalData.userSig = res.result.resData.userSig;
                app.globalData.isNotLogin = false;
                app.globalData.isTimUpdate = res.result.resData.isTimUpdate;
                wx.setStorageSync('userInfo', res.result.resData.userInfo);
                wx.setStorageSync('isTimUpdate', res.result.resData.isTimUpdate);
                wx.setStorageSync('userID', res.result.resData.userID);
                wx.setStorageSync('userSig', res.result.resData.userSig);
                // 登录 TIM
                this.login();
            }, err => {
                // databaseApi.hasUser 调用失败
                console.log('databaseApi.login: err ==> ', err);
                setTimeout(() => {
                    this.$showMsg('系统繁忙，请稍候再试！', 'error');
                }, 3000);
            }).finally(() => {
                // 登录|注册 完毕，跳转到上一级页面
                return wx.navigateBack({
                  delta: 1,
                });
            }).then(res => {
                console.log('navigateBack: ok ==> ', res);
            }, err => {
                console.log('navigateBack: err ==> ', err);
            });
            // .finally(() => {
            //     console.log('========== 登录 ==========');
            //     console.log('globalData.userInfo ==> ', app.globalData.userInfo);
            //     console.log('globalData.isNotLogin ==> ', app.globalData.isNotLogin);
            //     console.log('storage.userInfo ==>', wx.getStorageSync('userInfo'));
            //     console.log('storage.userID ==>', wx.getStorageSync('userID'));
            //     console.log('storage.userSig ==>', wx.getStorageSync('userSig'));
            // });
        }
    },
    /**
     * 协议选择的处理事件
     * @param {*} e 
     */
    onCheckboxChange(e) {
        if (e.detail.value[0] === 'agree') {
            this.setData({
                isAgree: true
            });
        } else {
            this.setData({
                isAgree: false
            });
        }
    },
    /**
     * 控制协议的显隐
     */
    onShowXieyi() {
        this.setData({
            isShow: true
        });
    },
    /**
     * 登录TIM
     */
    login() {
        // 从全局数据里取出数据
        const userID = app.globalData.userID;
        const userSig = app.globalData.userSig;
        // logger.log(`TUI-login | login  | userSig:${userSig} userID:${userID}`)
        wx.$TUIKit.login({
            userID: userID,
            userSig: userSig
        }).then(res => {
            // console.log('login res ==>', res);
            app.globalData.isTimLogin = true;
        }).catch((err) => {});
    },
    /**
     * 显示信息
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    }
})