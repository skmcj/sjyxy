// pages/idea/idea.js
const db = wx.cloud.database();

Page({
    /**
     * 页面的初始数据
     */
    data: {
        // 已输入反馈字数
        num: 0,
        // 反馈内容
        content: ''
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {},
    // 获取输入框的值
    getDataBindTap: function (e) {
        var information = e.detail.value; //输入的内容
        var value = e.detail.value.length; //输入内容的长度
        var lastArea = 150 - value; //剩余字数
        var that = this;
        that.setData({
            information: information,
            lastArea: lastArea
        })
    },
    /**
     * 监听输入
     * @param {*} e 
     */
    changeIdea(e) {
        this.setData({
            num: e.detail.cursor,
            content: e.detail.value
        });
        // console.log('data ==>', this.data.content, this.data.num);
    },
    /**
     * 提交
     */
    onSubmit() {
        if (this.data.num > 0) {
            // 存储数据库
            db.collection('feedback').add({
                data: {
                    createTime: this.getTimeFormat(new Date()),
                    content: this.data.content
                }
            }).then(res => {
                console.log('add feedback success ==>', res);
                wx.redirectTo({
                    url: '/pages/order_successful/order_successful?idea=意见反馈'
                });
            }, err => {
                console.log('add feesback error ==>', err);
            });
        } else {
            this.$showMsg('请输入反馈内容');
        }

        
    },
    /**
     * 获取yyyy-MM-dd格式的时间
     * @param {Date} date 
     */
    getTimeFormat(date) {
        return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    },
    /**
     * 查看历史反馈
     */
    onHistory() {
        wx.navigateTo({
            url: '/pages/idea_history/idea_history'
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {},
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {},
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {},
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {},
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {},
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {},
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {},
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
            title: msg,
            icon: type
        });
    }
})