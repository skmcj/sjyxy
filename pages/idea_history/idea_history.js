// pages/idea_see/idea_see.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        hasIdea: false,
        ideaList: [],
        pageNum: 1,
        pageSize: 6,
        total: 0,
        isLoading: false
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {},
    /**
     * 获取历史意见
     * @param {*} pageNum 
     */
    getIdeaList(pageNum) {
        wx.showLoading({
            title: '加载中',
          });
          wx.cloud.callFunction({
              name: 'databaseApi',
              data: {
                  type: 'getFeedback',
                  reqData: {
                      pageSize: this.data.pageSize,
                      pageNum: pageNum
                  }
              }
          }).then(res => {
              // console.log('getIdeaList success ==>', res);
              if(res.result.resData.data) {
                  // 刷新状态
                  this.setData({
                      ['ideaList[' + (pageNum - 1) + ']']: res.result.resData.data,
                      hasIdea: true,
                      total: res.result.resData.total
                  });
              }
          }, err => {
              console.log('getIdeaList error ==>', err);
          }).finally(() => {
              this.data.isLoading = false;
              wx.hideLoading();
          });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getIdeaList(1);
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
        this.setData({
            hasIdea: false,
            ideaList: [],
            pageNum: 1
        });
        this.getIdeaList(1);
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        if(!this.data.isLoading && this.data.pageNum * this.data.pageSize < this.data.total) {
            this.data.isLoading = true;
            this.data.pageNum += 1;
            this.getIdeaList(this.data.pageNum);
        }else {
            this.$showMsg('没有更多意见了');
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    }
})