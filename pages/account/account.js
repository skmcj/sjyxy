// pages/account/account.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        income: 0,              // 总收入
        pay: 0,                 // 总支出
        orderCount: 0,          // 完成订单量
        recycleProfit: 0,       // 回收收益
        idleProfit: 0,          // 闲置收益
        integral: 0             // 积分
    },
    /**
     * 获取用户统计信息
     */
    getAccount() {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getUserAccount'
            }
        }).then(res => {
            // console.log('getUserAccount success ==>', res);
            this.setData({
                income: res.result.resData.income,               // 总收入
                pay: res.result.resData.pay,                     // 总支出
                orderCount: res.result.resData.orderCount,       // 完成订单量
                recycleProfit: res.result.resData.recycleProfit, // 回收收益
                idleProfit: res.result.resData.idleProfit,       // 闲置收益
                integral: res.result.resData.integral            // 积分
            });
        }, err => {
            console.log('getUserAccount error ==>', err);
        });
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getAccount();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})