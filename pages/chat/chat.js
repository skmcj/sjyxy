// pages/chat/chat.js
import logger from '../../utils/logger';
const app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        conversationName: '',
        conversation: {},
        messageList: [],
        isShow: false,
        showImage: false,
        showChat: true,
        conversationID: '',
        config: {
            sdkAppID: '',
            userID: '',
            userSig: '',
            type: 1,
            tim: null,
        },
        unreadCount: 0,
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        const {
            config
        } = this.data;
        config.sdkAppID = app.globalData.SDKAppID;
        config.userID = app.globalData.userInfo.userID;
        config.userSig = app.globalData.userInfo.userSig;
        config.tim = wx.$TUIKit;
        this.setData({
            config,
        });
        // conversationID: C2C、 GROUP
        logger.log(`| TUI-chat | onLoad | conversationInfomation: ${options.conversationInfomation}`);
        const payloadData = JSON.parse(options.conversationInfomation);
        const unreadCount = payloadData.unreadCount ? payloadData.unreadCount : 0;
        this.setData({
            conversationID: payloadData.conversationID,
            unreadCount,
        });
        wx.$TUIKit.setMessageRead({
            conversationID: this.data.conversationID
        }).then(() => {
            logger.log('| TUI-chat | setMessageRead | ok');
        });
        console.log('coversationID ==>', this.data.conversationID);
        wx.$TUIKit.getConversationProfile(this.data.conversationID).then(res => {
            console.log('profile ==>', res);
            const {
                conversation
            } = res.data;
            this.setData({
                conversationName: this.getConversationName(conversation),
                conversation,
                isShow: conversation.type === 'GROUP',
            });
            // console.log('name ==>', this.data.conversationName);
            wx.setNavigationBarTitle({
              title: this.data.conversationName,
            });
        });
    },
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {},
    getConversationName(conversation) {
        if (conversation.type === '@TIM#SYSTEM') {
            this.setData({
                showChat: false,
            });
            return '系统通知';
        }
        if (conversation.type === 'C2C') {
            return conversation.remark || conversation.userProfile.nick || conversation.userProfile.userID;
        }
        if (conversation.type === 'GROUP') {
            return conversation.groupProfile.name || conversation.groupProfile.groupID;
        }
    },
    sendMessage(event) {
        // 将自己发送的消息写进消息列表里面
        this.selectComponent('#message-list').updateMessageList(event.detail.message);
    },
    showMessageErrorImage(event) {
        this.selectComponent('#message-list').sendMessageError(event);
    },
    triggerClose() {
        this.selectComponent('#message-input').handleClose();
    },
    changeMemberCount(event) {
        this.selectComponent('#group-profile').updateMemberCount(event.detail.groupOptionsNumber);
    },
    resendMessage(event) {
        this.selectComponent('#message-input').onInputValueChange(event);
    },
});