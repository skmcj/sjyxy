// pages/a_key_order/a_key_order.js
const app = getApp();

Page({
    /**
     * 页面的初始数据
     */
    data: {
        // 选择地址
        address: {},
        // 有无地址
        hasAddress: false,
        // 协议显隐
        termsShow: false,
        // 所选重量编号
        _quantityIndex: 0,
        isNotLogin: true
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {},
    /**
     * 预估重量选择
     * @param {*} e 
     */
    onSelectQuantity(e) {
        // console.log('index ==>', e.currentTarget.dataset.index);
        let index = e.currentTarget.dataset.index;
        this.setData({
            _quantityIndex: index
        });
    },
    /**
     * 选择地址
     */
    onSelectAddress() {
        if(this.data.isNotLogin) {
            this.$showMsg('登录后可使用功能');
        }else {
            // 选择地址
            wx.navigateTo({
                url: '/pages/address_administration/address_administration',
            }).then(res => {
                console.log('address to new res ==> ', res);
            }, err => {
                console.log('address to new err ==> ', err);
            });
        }
    },
    /**
     * 检查是否有选择地址
     */
    checkSelectAddress() {
        let selectAddress = wx.getStorageSync('recycleAddress');
        if (selectAddress !== '') {
            this.setData({
                address: selectAddress,
                hasAddress: true
            });
            // wx.removeStorageSync('recycleAddress');
        } else {
            this.setData({
                address: {},
                hasAddress: false
            });
        }
    },
    /**
     * 显示服务条款
     */
    onShowTerms() {
        this.setData({
            termsShow: true
        });
    },
    /**
     * 点击下单按钮
     * @param {Object} e 
     */
    onSelectOrder(e) {
        if(this.data.isNotLogin) {
            this.$showMsg('登录后可使用功能');
        }else {
            if (this.data.hasAddress) {
                // 如果已选地址
                // 设置传输参数
                let index = e.currentTarget.dataset.type;
                let type = index ? '立即下单' : '公益赠送';
                let money = this.getEstimatedPrice(this.data._quantityIndex);
                let recycleType = {
                    oid: this.data._quantityIndex,
                    otype: type,
                    oMoney: money
                }
                wx.setStorageSync('recycleType', recycleType);
                // let addressStr = JSON.stringify(this.data.address);
                wx.navigateTo({
                    url: '/pages/place_order/place_order'
                }).then(res => {
                    console.log('page: recycle to place_order success');
                }, err => {
                    console.log('page: recycle to place_order fail');
                });
            } else {
                // 如果未选地址
                this.$showMsg('请添加地址');
            }
        }
        
    },
    /**
     * 计算预估价格
     */
    getEstimatedPrice(hIndex) {
        /** 根据所选预估重量计算预估价格
         * [2-6]  — (2-36)
         * [4-10] — (25-56)
         * [8-12] — (32-64)
         * [10+]  — (45-96)
         */
        let pList = [
            {region: 32, inc: 2},
            {region: 31, inc: 25},
            {region: 32, inc: 32},
            {region: 51, inc: 45}
        ];
        let orderMoney = Math.floor(Math.random() * pList[hIndex].region) + pList[hIndex].inc;
        return orderMoney;
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.setData({
            isNotLogin: app.globalData.isNotLogin
        });
        this.checkSelectAddress();
    },
    /**
     * 显示消息提示
     * @param {String} msg 消息体
     * @param {String} type 提示类型[loading|success|error|none]
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
            title: msg,
            icon: type
        });
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {},

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})