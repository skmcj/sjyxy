// pages/my/my.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        isNotLogin: true,
        userInfo: '',
        defaultAvatar: 'cloud://sjyxy-4g3ywpxce25a5af5.736a-sjyxy-4g3ywpxce25a5af5-1310485181/common/my/no_user.svg',
        account: ''
    },
    login() {
        wx.navigateTo({
            url: '/pages/login/login',
        }).then(res => {
            // console.log('跳转到登录页');
        }, err => {
            console.log('跳转到登录页失败');
        });
    },
    /**
     * 点击查看个人信息
     */
    onToPersonalInfo() {
        if(this.data.userInfo) {
            // 已登录
            wx.navigateTo({
              url: '/pages/personal_info/personal_info',
            });
        }else {
            // 未登录
            wx.navigateTo({
              url: '/pages/login/login',
            });
        }
    },
    /**
     * 获取用户统计信息
     */
    getAccount() {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getUserAccount'
            }
        }).then(res => {
            // console.log('getUserAccount success ==>', res);
            this.setData({
                account: res.result.resData
            });
        }, err => {
            console.log('getUserAccount error ==>', err);
        });
    },
    /**
     * 点击查看订单
     */
    onToCkdd() {
        if(!this.data.isNotLogin) {
            // 已登录
             wx.navigateTo({
                url: '/pages/view_order/view_order',
            }); 
        }else {
            // 未登录
            this.$showMsg('登录以使用相应功能');
        }
        
    },
    /**
     * 点击账户统计
     */
    onToZhtj() {
        if(!this.data.isNotLogin) {
            wx.navigateTo({
                url: '/pages/account/account'
            });
        }else {
            this.$showMsg('登录以使用相应功能');
        }
        
    },
    /**
     * 点击地址管理
     */
    onToDzgl() {
        if(!this.data.isNotLogin) {
            wx.navigateTo({
                url: '/pages/address_administration/address_administration',
            });
        }else {
            this.$showMsg('登录以使用相应功能');
        }
        
    },
    /**
     * 点击闲置商城
     */
    onToXzsc() {
        if(!this.data.isNotLogin) {
            wx.navigateTo({
                url: '/subpkg/idlemall/idlemall',
            });
        }else {
            this.$showMsg('登录以使用相应功能');
        } 
    },
    /**
     * 点击积分商城
     */
    onToJfsc() {
        if(!this.data.isNotLogin) {
            wx.navigateTo({
                url: '/subpkg/integralmall/index/index',
            });
        }else {
            this.$showMsg('登录以使用相应功能');
        }  
    },
    /**
     * 点击意见反馈
     */
    onToYjfk() {
        if(!this.data.isNotLogin) {
            wx.navigateTo({
                url: '/pages/idea/idea'
            }); 
        }else {
            this.$showMsg('登录以使用相应功能');
        }
    },
    /**
     * 获取登录用户信息
     */
    getUserInfo() {
        let userInfo = wx.getStorageSync('userInfo');
        if(userInfo) {
            this.setData({
                userInfo: userInfo,
                isNotLogin: false
            });
        }
        // console.log('userInfo ==>', this.data.userInfo);
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // this.getUserInfo();
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {},
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        // console.log('my show');
        this.getUserInfo();
        this.getAccount();
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        // console.log('mu hide');
    },
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {},
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {},
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {},
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {},
    /**
     * 展示提示
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    },
})