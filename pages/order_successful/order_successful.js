// pages/order_successful/order_successful.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        idea: ''
    },

    /**
     * 跳转到订单列表页
     */
    onNavToOrders() {
        // 导航到回收订单页面
        wx.redirectTo({
          url: '/pages/view_order/view_order?titId=1',
        }).then(res => {
            // 跳转成功
            // console.log('order_success to orderList success', res);
        }, err => {
            // 跳转失败
            console.log('order_success to orderList fail', err);
        });
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // 设置标题
        if(options.type) {
            wx.setNavigationBarTitle({
              title: options.type,
            });
        }
        if(options.idea) {
            this.setData({
                idea: options.idea
            });
            wx.setNavigationBarTitle({
              title: this.data.idea,
            });
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})