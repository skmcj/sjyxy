const app = getApp();

Page({
    data: {},
    onLoad(options) {
        this.isTimLogin();
    },
    /**
     * 判断是否登录
     */
    isTimLogin() {
        if(!app.globalData.isNotLogin && !app.globalData.isTimLogin) {
            this.timLogin();
        }
    },
    /**
     * 登录 TIM
     */
    timLogin() {
        // 从全局数据里取出数据
        const userID = app.globalData.userID;
        const userSig = app.globalData.userSig;
        // logger.log(`TUI-login | login  | userSig:${userSig} userID:${userID}`)
        wx.$TUIKit.login({
            userID: userID,
            userSig: userSig
        }).then(res => {
            // console.log('login res ==>', res);
            app.globalData.isTimLogin = true;
        }).catch((err) => {});
    },
    /**
     * 跳转到闲置商城
     */
    onNavToIdleMall() {
        if(!app.globalData.isNotLogin) {
            wx.navigateTo({
                url: '/subpkg/idlemall/idlemall',
            }).then(res => {
                // 跳转成功
                // console.log('index to idlemall success ==>', res);
            }, err => {
                // 跳转失败
                console.log('index to idlemall fail ==>', err);
            });
        }else {
            this.$showMsg('登录以使用相应功能');
        }
        
    },
    /**
     * 跳转到积分商城
     */
    onNavToIntegralMall() {
        if(!app.globalData.isNotLogin) {
            wx.navigateTo({
                url: '/subpkg/integralmall/index/index',
            }).then(res => {
                // 跳转成功
                // console.log('index to integralmall success ==>', res);
            }, err => {
                // 跳转失败
                console.log('index to integralmall fail ==>', err);
            });
        }else {
            this.$showMsg('登录以使用相应功能');
        } 
    },
    /**
     * 跳转到一键回收页面
     */
    onNavToRecycle() {
        wx.switchTab({
          url: '/pages/recycle/recycle',
        }).then(res => {
            // 跳转成功
            // console.log('index to recycle success ==>', res);
        }, err => {
            // 跳转失败
            console.log('index to recycle fail ==>', err);
        });
    },
    /**
     * 帮助中心跳转 
     */
    helpCenter() {
        wx.navigateTo({
            url: '/pages/helpCenter/helpCenter'
        });
    },
    /**
     * 展示提示
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    }
})