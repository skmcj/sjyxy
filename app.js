// app.js
// 腾讯IM sdk
import TIM from './static/plugin/tim-wx';
// 腾讯云即时通信 IM 上传插件
import TIMUploadPlugin from './static/plugin/tim-upload-plugin';
// 日志
// import logger from './utils/logger';
// 导入SDKAPPID
import {
    SDKAPPID
} from './static/plugin/root-data';

App({
    onLaunch() {
        wx.removeStorageSync('recycleAddress');
        this.initGlobalData();
        // 展示本地存储能力
        wx.cloud.init({
            env: '自己的云环境ID',
            traceUser: true
        });
        this.initTIM();
    },
    /**
     * 初始化全局数据
     */
    initGlobalData() {
        this.globalData.userInfo = wx.getStorageSync('userInfo');
        this.globalData.userID = wx.getStorageSync('userID');
        this.globalData.userSig = wx.getStorageSync('userSig');
        this.globalData.isTimUpdate = wx.getStorageSync('isTimUpdate');
        if (this.globalData.userInfo === '' || JSON.stringify(this.globalData.userInfo) === '{}') {
            this.globalData.isNotLogin = true;
        } else {
            this.globalData.isNotLogin = false;
        }
    },
    /**
     * 初始化TIM服务
     */
    initTIM() {
        // 获取SDKAPPID
        // const SDKAppID = this.globalData.SDKAppID;
        // wx.setStorageSync(`TIM_${SDKAppID}_isTUIKit`, true);
        // 创建实例
        wx.$TUIKit = TIM.create({
            SDKAppID: SDKAPPID
        });
        /* 注册腾讯云即时通信 IM 上传插件,
         * 即时通信 IM SDK 发送图片、语音、视频、文件等消息需要使用上传插件,
         * 将文件上传到腾讯云对象存储 
         */
        wx.$TUIKit.registerPlugin({
            'tim-upload-plugin': TIMUploadPlugin
        });
        wx.$TUIKitTIM = TIM;
        wx.$TUIKitEvent = TIM.EVENT;
        wx.$TUIKitVersion = TIM.VERSION;
        wx.$TUIKitTypes = TIM.TYPES;
        // 监听系统级事件
        // 收到 SDK 进入 not ready 状态通知，此时 SDK 无法正常工作
        // wx.$TUIKit.on(wx.$TUIKitEvent.SDK_NOT_READY, this.onSdkNotReady);
        // 收到被踢下线通知
        wx.$TUIKit.on(wx.$TUIKitEvent.KICKED_OUT, this.onKickedOut);
        // 收到 SDK 发生错误通知，可以获取错误码和错误信息
        wx.$TUIKit.on(wx.$TUIKitEvent.ERROR, this.onTIMError);
        // 网络状态发生改变
        // wx.$TUIKit.on(wx.$TUIKitEvent.NET_STATE_CHANGE, this.onNetStateChange);
        // wx.$TUIKit.on(wx.$TUIKitEvent.SDK_RELOAD, this.onSDKReload);
        // 收到离线消息和会话列表同步完毕通知，接入侧可以调用 sendMessage 等需要鉴权的接口
        wx.$TUIKit.on(wx.$TUIKitEvent.SDK_READY, this.onSDKReady);
    },
    // 监听事件
    onSDKReady(e) {
        console.log('<=============== ready sdk ===============>');
        if(!this.globalData.isTimUpdate) {
            wx.$TUIKit.updateMyProfile({
                nick: this.globalData.userInfo.nickName,
                avatar: this.globalData.userInfo.avatarUrl,
            }).then(res => {
                // 修改 TIM 信息成功
                this.globalData.isTimUpdate = true;
                // 更新数据库用户信息
                return wx.cloud.callFunction({
                    name: 'databaseApi',
                    data: {
                        type: 'updateUser',
                        reqData: {
                            isTimUpdate: true
                        }
                    }
                });
            }, err => {
                // 修改 TIM 信息失败
                console.error('update TIM userProfile ==>', err);
            }).then(res => {
                // 修改用户数据成功
                // console.log('update user isTimUpdate success ==>', res);
                wx.setStorageSync('isTimUpdate', true);
            }, err => {
                // 修改用户数据失败
                console.error('update user isTimUpdate error ==>', err);
            });
        }
    },
    /**
     * 收到 SDK 进入 not ready 状态通知，此时 SDK 无法正常工作
     * @param {*} e 
     */
    onSdkNotReady(e) {},
    /**
     * 收到被踢下线通知
     * event.name - TIM.EVENT.KICKED_OUT
     * event.data.type - 被踢下线的原因，例如 :
     *   - TIM.TYPES.KICKED_OUT_MULT_ACCOUNT 多实例登录被踢
     *   - TIM.TYPES.KICKED_OUT_MULT_DEVICE 多终端登录被踢
     *   - TIM.TYPES.KICKED_OUT_USERSIG_EXPIRED 签名过期被踢（v2.4.0起支持）。
     * @param {*} e 
     */
    onKickedOut(e) {
        wx.showToast({
            title: '您被踢下线',
            icon: 'error',
        });
    },
    /**
     * 收到 SDK 发生错误通知，可以获取错误码和错误信息
     *   - event.name - TIM.EVENT.ERROR
     *   - event.data.code - 错误码
     *   - event.data.message - 错误信息
     * @param {*} e 
     */
    onTIMError(e) {},
    /**
     * 网络状态发生改变（v2.5.0 起支持）
     *   - event.name - TIM.EVENT.NET_STATE_CHANGE
     *   - event.data.state 当前网络状态，枚举值及说明如下：
     *     - TIM.TYPES.NET_STATE_CONNECTED - 已接入网络
     *     - TIM.TYPES.NET_STATE_CONNECTING - 连接中。很可能遇到网络抖动，SDK 在重试。接入侧可根据此状态提示“当前网络不稳定”或“连接中”
     *     - TIM.TYPES.NET_STATE_DISCONNECTED - 未接入网络。接入侧可根据此状态提示“当前网络不可用”。SDK 仍会继续重试，若用户网络恢复，SDK 会自动同步消息
     * @param {*} e 
     */
    onNetStateChange(e) {},
    onSDKReload(e) {},
    /**
     * 登录TIM
     */
    login() {
        console.log('login data ==>', this.globalData);
        // 从全局数据里取出数据
        const userID = this.globalData.userID;
        const userSig = this.globalData.userSig;
        // logger.log(`TUI-login | login  | userSig:${userSig} userID:${userID}`)
        wx.$TUIKit.login({
            userID: userID,
            userSig: userSig
        }).then(res => {
            console.log('login res ==>', res);
            this.globalData.isTimLogin = true;
        }).catch((err) => {});
    },
    globalData: {
        isTimLogin: false,
        isTimUpdate: false,
        isNotLogin: true,
        userInfo: {},
        userID: '',
        userSig: ''
    }
})
