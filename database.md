# 数据库设计

## 用户表-users

| 字段名        | 类型      | 备注                                |
| ------------- | --------- | ----------------------------------- |
| `_id`         | `String`  | 记录唯一标识，这里为用户的`_openid` |
| `_id_`        | `String`  | 与`_id`一致，用于云函数自定义`_id`  |
| `_openid`     | `String`  | 用户的`_openid`                     |
| `account`     | `Object`  | 用户的账户统计                      |
| `isTimUpdate` | `Boolean` | 记录用户信息是否同步于**IM**        |
| `userID`      | `String`  | 用户**IM**服务的`userID`            |
| `userInfo`    | `Object`  | 用户信息                            |

- `account`字段属性如下

  | 属性名           | 类型     | 备注         |
  | ---------------- | -------- | ------------ |
  | `income`         | `Number` | 总收入       |
  | `pay`            | `Number` | 总支出       |
  | `orderCount`     | `Number` | 完成订单量   |
  | `recycleProfit`  | `Number` | 回收订单收益 |
  | `idleProfit`     | `Number` | 闲置商品收益 |
  | `integral`       | `Number` | 积分         |
  | `reduceIntegral` | `Number` | 累计消耗积分 |

- `userInfo`字段属性如下

  | 属性名      | 类型     | 备注                         |
  | ----------- | -------- | ---------------------------- |
  | `acatarUrl` | `Number` | 用户头像                     |
  | `nickName`  | `Number` | 用户昵称                     |
  | `gender`    | `Number` | 用户性别(0-未知\|1-男\|2-女) |
  | `language`  | `Number` | 用户语言                     |
  | `country`   | `Number` | 用户所在国家                 |
  | `city`      | `Number` | 用户所在城市                 |
  | `province`  | `Number` | 用户所在省                   |

## 地址表-address

| 字段名    | 类型     | 备注                                |
| --------- | -------- | ----------------------------------- |
| `_id`     | `String` | 记录唯一标识，这里为用户的`_openid` |
| `_id_`    | `String` | 与`_id`一致，用于云函数自定义`_id`  |
| `_openid` | `String` | 用户的`_openid`                     |
| `address` | `Array`  | 用户添加的地址                      |

- `address`每一项都为一个**地址对象**，属性如下

  | 属性名     | 类型      | 备注                 |
  | ---------- | --------- | -------------------- |
  | `name`     | `String`  | 姓名                 |
  | `sex`      | `String`  | 性别称呼(先生\|女士) |
  | `phone`    | `String`  | 联系方式             |
  | `location` | `String`  | 定位信息             |
  | `desc`     | `String`  | 详细地址             |
  | `default`  | `Boolean` | 是否为默认地址       |

## 订单消息表-order_mess

| 字段名        | 类型     | 备注                                   |
| ------------- | -------- | -------------------------------------- |
| `_id`         | `String` | 记录唯一标识，这里为用户的`_openid`    |
| `_id_`        | `String` | 与`_id`一致，用于云函数自定义`_id`     |
| `_openid`     | `String` | 用户的`_openid`                        |
| `recycleMess` | `Array`  | 用户回收订单，状态变化时产生的消息     |
| `shopMess`    | `Array`  | 用户购买商品时，订单状态变化产生的信息 |

- `recycleMess`的`item`对象属性如下

  | 属性名            | 类型      | 备注                             |
  | ----------------- | --------- | -------------------------------- |
  | `createTime`      | `String`  | 消息创建时间                     |
  | `createTimeStamp` | `Number`  | 消息创建时间对应时间戳           |
  | `isRead`          | `Boolean` | 是否已读                         |
  | `orderAmount`     | `String`  | 订单预览，回收订单的重量         |
  | `orderId`         | `String`  | 订单ID                           |
  | `orderState`      | `Number`  | 订单状态(已预约\|已上门\|已完成) |

- `shopMess`的`item`对象属性如下

  | 属性名            | 类型      | 备注                                                         |
  | ----------------- | --------- | ------------------------------------------------------------ |
  | `createTime`      | `String`  | 消息创建时间                                                 |
  | `createTimeStamp` | `Number`  | 消息创建时间对应时间戳                                       |
  | `goodsName`       | `String`  | 商品名称                                                     |
  | `isRead`          | `Boolean` | 是否已读                                                     |
  | `orderAvatar`     | `String`  | 订单预览，商品图片                                           |
  | `orderId`         | `String`  | 订单ID                                                       |
  | `orderState`      | `Number`  | 订单状态(待支付\|待发货\|已发货\|售后中\|已完成\|待退款\|已退款) |

## 回收订单表-recycle_order

| 字段名      | 类型     | 备注           |
| ----------- | -------- | -------------- |
| `_id`       | `String` | 订单ID         |
| `_openid`   | `String` | 用户的`openid` |
| `orderForm` | `Object` | 订单表单信息   |

- `orderForm`属性如下

  | 属性名           | 类型     | 备注                               |
  | ---------------- | -------- | ---------------------------------- |
  | `address`        | `Object` | 订单地址对象，与上面的地址对象一致 |
  | `ammount`        | `Number` | 订单预估重量编号                   |
  | `amountStr`      | `String` | 订单预估重量                       |
  | `createTime`     | `String` | 订单创建时间                       |
  | `estimatedPrice` | `Number` | 预估价格                           |
  | `orderCode`      | `String` | 订单号                             |
  | `orderTime`      | `String` | 预约上门时间                       |
  | `orderType`      | `String` | 订单类型                           |
  | `remark`         | `String` | 订单备注                           |
  | `state`          | `Number` | 订单状态(已预约\|已上门\|已完成) |

## 闲置商品表-idlemallSend

| 字段名        | 类型     | 备注                             |
| ------------- | -------- | -------------------------------- |
| `_id`         | `String` | 商品ID                           |
| `_openid`     | `String` | 用户的`openid`                   |
| `goodsData`   | `Object` | 商品信息                         |
| `goodsImg`    | `Array`  | 商品图片数组                     |
| `goodsStatus` | `String` | 商品状态(出售中\|已下架\|无库存) |
| `goodsType`   | `String` | 商品类型(书籍\|用品\|箱包\|其它) |

- `goodsData`属性如下

  | 属性名        | 类型     | 备注                 |
  | ------------- | -------- | -------------------- |
  | `goodsBefore` | `String` | 商品原价             |
  | `goodsDetail` | `String` | 商品描述             |
  | `goodsName`   | `String` | 商品名               |
  | `goodsNum`    | `Number` | 商品库存             |
  | `goodsPrice`  | `String` | 商品现价             |
  | `goodsWay`    | `String` | 配送方式(配送\|自提) |

## 闲置商品订单表-idlemallOrder

| 字段名         | 类型     | 备注                                                         |
| -------------- | -------- | ------------------------------------------------------------ |
| `_id`          | `String` | 记录ID                                                       |
| `_openid`      | `String` | 用户的`openid`                                               |
| `address`      | `Object` | 订单地址                                                     |
| `amount`       | `Number` | 商品数量                                                     |
| `createTime`   | `String` | 订单创建时间                                                 |
| `goodsMess`    | `Object` | 商品信息                                                     |
| `itemNo`       | `String` | 商品ID                                                       |
| `orderCode`    | `String` | 订单号                                                       |
| `orderDetail`  | `String` | 订单备注                                                     |
| `orderStatus`  | `Number` | 订单状态(待支付\|待发货\|已发货\|售后中\|已完成\|待退款\|已退款) |
| `payTime`      | `String` | 支付时间                                                     |
| `payment`      | `String` | 支付金额                                                     |
| `refundReason` | `Object` | 退款原因                                                     |

- `address`对象与前面的地址表一致

- `goodsMess`属性大致如下

  | 属性名        | 类型     | 备注               |
  | ------------- | -------- | ------------------ |
  | `goodsData`   | `Object` | 商品信息           |
  | `goodsImg`    | `Array`  | 商品图片列表       |
  | `goodsStatus` | `String` | 商品状态           |
  | `goodsType`   | `String` | 商品类型           |
  | `userID`      | `String` | 商品发布者`userID` |
  | `userInfo`    | `String` | 商品发布者信息     |

- `refundReason`属性如下，当用户退款才会出现此字段

  | 属性名       | 类型     | 备注         |
  | ------------ | -------- | ------------ |
  | `reasonDesc` | `String` | 退款详细描述 |
  | `reasonText` | `String` | 退款原因     |

## 积分商品表-shoppingList

该功能未设计好，后续有机会再完善

| 字段名     | 类型     | 备注         |
| ---------- | -------- | ------------ |
| `_id`      | `String` | 商品ID       |
| `cate`     | `String` | 商品类型     |
| `img`      | `String` | 商品图片     |
| `integral` | `Number` | 兑换所需积分 |
| `name`     | `String` | 商品名称     |

## 反馈信息表-feedback

| 字段名       | 类型     | 备注           |
| ------------ | -------- | -------------- |
| `_id`        | `String` | 反馈信息ID     |
| `_openid`    | `String` | 用户的`openid` |
| `content`    | `String` | 反馈内容       |
| `createTime` | `String` | 记录创建时间   |

