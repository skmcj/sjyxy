// components/yxy-chat/mess-elements/mess-video/mess-video.js
import { parseVideo } from '../../../base/message-facade.js';

Component({
    /**
     * 组件的属性列表
     */
    properties: {
        message: {
            type: Object,
            value: {},
            observer(newVal) {
                this.setData({
                    message: newVal,
                    renderDom: parseVideo(newVal),
                });
            },
        },
        isMine: {
            type: Boolean,
            value: true,
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        message: {},
        showSaveFlag: Number,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        /**
         * 显示视频状态：全屏 | 非全屏
         * @param {*} event 
         */
        showVideoFullScreenChange(event) {
            if (event.detail.fullScreen) {
                this.setData({
                    showSaveFlag: 1,
                });
            } else {
                this.setData({
                    showSaveFlag: 2,
                });
            }
        },
        /**
         * 处理视频状态
         * 1代表当前状态处于全屏，2代表当前状态不处于全屏。
         * @param {*} e 
         */
        handleLongPress(e) {
            if (this.data.showSaveFlag === 1) {
                wx.showModal({
                    content: '确认保存该视频？',
                }).then(res => {
                    if (res.confirm) {
                        return wx.downloadFile({
                            url: this.data.message.payload.videoUrl,
                        });
                    }
                }, err => {})
                .then(res => {
                    // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
                    if (res.statusCode === 200) {
                        return wx.saveVideoToPhotosAlbum({
                            filePath: res.tempFilePath,
                        });
                    }
                }, err => {
                    wx.showToast({
                        title: '保存失败!',
                        duration: 800,
                        icon: 'none',
                    });
                }).then(res => {
                    wx.showToast({
                        title: '保存成功!',
                        duration: 800,
                        icon: 'none',
                    });
                }, err => {});
            }
        },
    }
})
