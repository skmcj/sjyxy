// components/yxy-chat/mess-elements/mess-image/mess-image.js
import { parseImage } from '../../../base/message-facade';

Component({
    /**
     * 组件的属性列表
     */
    properties: {
        message: {
            type: Object,
            value: {},
            observer(newVal) {
                this.setData({
                    renderDom: parseImage(newVal),
                    percent: newVal.percent,
                });
            },
        },
        isMine: {
            type: Boolean,
            value: true,
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        // 图片数组
        renderDom: [],
        // 百分比
        percent: 0,
        /** 是否保存 */
        showSave: false,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        /**
         *  预览图片
         */
        previewImage() {
            wx.previewImage({
                // 当前显示图片的http链接
                current: this.data.renderDom[0].src,
                // 图片链接必须是数组
                urls: [this.data.renderDom[0].src],
                success: () => {
                    this.setData({
                        showSave: true,
                    });
                },
                complete: () => {
                    this.setData({
                        showSave: false,
                    });
                },
            });
        },
    }
})
