// components/yxy-chat/mess-elements/emoji/emoji.js
import { emojiName, emojiUrl, emojiMap } from '../../../base/emojiMap';

Component({
    /**
     * 组件的属性列表
     */
    properties: {
        bgColor: {
            type: String,
            value: '#f5f3f2'
        }
    },
    /**
     * 组件的初始数据
     */
    data: {
        // 表情列表
        emojiList: []
    },
    /**
     * 页面的生命周期函数
     */
    lifetimes: {
        /**
         * 在组件进入页面节点树时执行
         */
        attached() {
            // 将emojiMap里的表情数据包装成可用对象
            for(let i = 0; i < emojiName.length; i++) {
                this.data.emojiList.push({
                    name: emojiName[i],
                    url: emojiUrl + emojiMap[emojiName[i]]
                });
            }
            this.setData({
                emojiList: this.data.emojiList
            });
        }
    },
    /**
     * 组件的方法列表
     */
    methods: {
        /**
         * 自定义事件 - 向页面传值(输入的emoji)
         * @param {*} e
         */
        handleEnterEmoji(e) {
            this.triggerEvent('enterEmoji', {
              message: e.currentTarget.dataset.name,
            });
        },
    }
})
