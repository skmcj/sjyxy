// components/yxy-chat/mess_input/mess-input.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        conversation: {
            type: Object,
            value: {},
            observer(newVal) {
                this.setData({
                    conversation: newVal,
                });
            },
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        // 会话类型
        conversation: {},
        // 输入框的内容
        message: '',
        // 切换语音输入时的提示文字
        text: '按住说话',
        // 发送消息按钮
        sendMessBtn: false,
        // 是否语音输入
        isAudio: false,
        // 声音录制相关参数
        startPoint: 0,
        // 语音录制提示框显隐
        popupToggle: false,
        isRecording: false,
        canSend: true,
        // 录音提示标题
        title: ' ',
        // notShow: false,
        // isShow: true,
        // 底部弹窗
        displayFlag: '',
    },
    lifetimes: {
        attached() {
            // 加载声音录制管理器
            this.recorderManager = wx.getRecorderManager();
            this.recorderManager.onStop(res => {
                wx.hideLoading();
                if (this.data.canSend) {
                    if (res.duration < 1000) {
                        wx.showToast({
                            title: '录音时间太短',
                            icon: 'none',
                        });
                    } else {
                        // res.tempFilePath 存储录音文件的临时路径
                        const message = wx.$TUIKit.createAudioMessage({
                            to: this.getToAccount(),
                            conversationType: this.data.conversation.type,
                            payload: {
                                file: res,
                            },
                        });
                        this.$sendTIMMessage(message);
                    }
                }
                this.setData({
                    startPoint: 0,
                    popupToggle: false,
                    isRecording: false,
                    canSend: true,
                    title: ' ',
                    text: '按住说话',
                });
            });
        },
    },
    /**
     * 组件的方法列表
     */
    methods: {
        /* <========== 左侧输入模式相关事件 ==========> */
        /**
         * 更改输入模式
         */
        switchMode() {
            this.setData({
                isAudio: !this.data.isAudio,
                text: '按住说话',
                displayFlag: ' '
            });
        },
        /**
         * 长按录音
         */
        handleLongPress(e) {
            this.recorderManager.start({
                duration: 60000,       // 录音的时长，单位 ms，最大值 600000（10 分钟）
                sampleRate: 44100,     // 采样率
                numberOfChannels: 1,   // 录音通道数
                encodeBitRate: 192000, // 编码码率
                format: 'aac',         // 音频格式，选择此格式创建的音频消息，可以在即时通信 IM 全平台（Android、iOS、微信小程序和Web）互通
            });
            console.log(e);
            this.setData({
                startPoint: e.touches[0],
                title: '正在录音',
                // isRecording : true,
                // canSend: true,
                // notShow: true,
                // isShow: false,
                isRecording: true,
                popupToggle: true,
            });
        },
        /**
         * 录音时的手势上划移动距离对应文案变化
         * @param {*} e 
         */
        handleTouchMove(e) {
            if (this.data.isRecording) {
                if (this.data.startPoint.clientY - e.touches[e.touches.length - 1].clientY > 100) {
                    this.setData({
                        text: '抬起停止',
                        title: '松开手指，取消发送',
                        canSend: false,
                    });
                } else if (this.data.startPoint.clientY - e.touches[e.touches.length - 1].clientY > 20) {
                    this.setData({
                        text: '抬起停止',
                        title: '上划可取消',
                        canSend: true,
                    });
                } else {
                    this.setData({
                        text: '抬起停止',
                        title: '正在录音',
                        canSend: true,
                    });
                }
            }
        },
        /**
         * 手指离开页面滑动
         */
        handleTouchEnd() {
            this.setData({
                isRecording: false,
                popupToggle: false,
                text: '按住说话'
            });
            wx.hideLoading();
            this.recorderManager.stop();
        },
        /* <========== 中间输入框相关事件 ==========> */
        /**
         * 监听键盘输入事件
         */
        onInputValueChange(e) {
            if (e.detail.message) {
                // 监听输入的 emoji
                this.setData({
                    message: e.detail.message.payload.text,
                    sendMessBtn: true,
                });
            } else if (e.detail.value) {
                // 监听输入文字
                this.setData({
                    message: e.detail.value,
                    sendMessBtn: true,
                });
            } else {
                this.setData({
                    message: e.detail.value,
                    sendMessBtn: false,
                });
            }
        },
        /**
         * 监听键盘点击完成事件
         */
        sendTextMessage(msg, flag) {
            const to = this.getToAccount();
            const text = flag ? msg : this.data.message;
            const message = wx.$TUIKit.createTextMessage({
                to,
                conversationType: this.data.conversation.type,
                payload: {
                    text,
                },
            });
            this.setData({
                message: '',
                sendMessBtn: false,
            });
            this.$sendTIMMessage(message);
        },
        /* <========== 右侧扩展相关事件 ==========> */
        /**
         * 选中表情消息
         */
        handleEmoji() {
            let targetFlag = 'emoji';
            if (this.data.displayFlag === 'emoji') {
                targetFlag = '';
            }
            this.setData({
                displayFlag: targetFlag,
                isAudio: false
            });
        },
        /**
         * 获取emoji的输入
         * @param {*} e 
         */
        appendMessage(e) {
            this.setData({
                message: this.data.message + e.detail.message,
                sendMessBtn: true,
            });
        },
        /**
         * 选自定义消息
         */
        handleExtensions() {
            let targetFlag = 'extension';
            if (this.data.displayFlag === 'extension') {
                targetFlag = '';
            }
            this.setData({
                displayFlag: targetFlag,
                isAudio: false
            });
        },
        /**
         * 相册操作
         */
        handleAlbum() {
            this.sendMediaMessage('album');
        },
        /**
         * 拍摄操作
         */
        handleShot() {
            this.sendMediaMessage('camera');
        },
        /**
         * 发送媒体文件
         * @param {*} type 
         */
        sendMediaMessage(type) {
            const maxSize = 10240000;
            wx.chooseMedia({
              sourceType: [type],
              maxDuration: 15,
              count: 1
            }).then(res => {
                console.log('res', res);
                if(res.type === 'image') {
                    if (res.tempFiles[0].size > maxSize) {
                        wx.showToast({
                            title: '大于10M图片不支持发送',
                            icon: 'none',
                        });
                        return;
                    }
                    const message = wx.$TUIKit.createImageMessage({
                        to: this.getToAccount(),
                        conversationType: this.data.conversation.type,
                        payload: {
                            file: this.packData(res, 'image'),
                        },
                        onProgress: (percent) => {
                            message.percent = percent;
                        },
                    });
                    this.$sendTIMMessage(message);
                }else {
                    const message = wx.$TUIKit.createVideoMessage({
                        to: this.getToAccount(),
                        conversationType: this.data.conversation.type,
                        payload: {
                            file: this.packData(res, 'video'),
                        },
                        onProgress: (percent) => {
                            message.percent = percent;
                        },
                    });
                    this.$sendTIMMessage(message);
                }
            }, err => {
                // 发送图片|视频 失败
                console.log('sendMedia fail ==>', err);
            });
        },
        /**
         * 处理语音 | 视频 通话
         * 暂不加入
         * @param {*} e 
         */
        handleCalling(e) {
            // todo 目前支持单聊
            if (this.data.conversation.type === 'GROUP') {
                wx.showToast({
                    title: '群聊暂不支持',
                    icon: 'none',
                });
                return;
            }
            const type = e.currentTarget.dataset.value;
            const {
                userID
            } = this.data.conversation.userProfile;
            this.triggerEvent('handleCall', {
                type,
                userID,
            });
            this.setData({
                displayFlag: '',
            });
        },
        /* <========== 一些辅助事件 ==========> */
        /**
         * 包装数据
         *  - 一个 sb 方法，为的是将小程序新接口获取的数据“做旧”为旧接口返回的数据，以适应 tim-wx 的参数需求
         * @param {*} data 主要为 wx.chooseMedia 返回的数据
         * @param {*} type 类型：'image' | 'video'
         * @returns {Object} 返回 wx.chooseImage 或 wx.chooseVideo 接口返回类型的数据
         */
        packData(data, type) {
            let res = {};
            if (type === 'image') {
                res.tempFilePaths = [];
                res.tempFiles = [];
                for(let i = 0; i < data.tempFiles.length; i++) {
                    res.tempFilePaths.push(data.tempFiles[i].tempFilePath);
                    let image = {};
                    image.path = data.tempFiles[i].tempFilePath;
                    image.size = data.tempFiles[i].size;
                    res.tempFiles.push(image);
                }
            } else if (type === 'video') {
                res.tempFilePath = data.tempFiles[0].tempFilePath;
                res.duration = data.tempFiles[0].duration;
                res.size = data.tempFiles[0].size;
                res.width = data.tempFiles[0].width;
                res.height = data.tempFiles[0].height;
            }
            return res;
        },
        /**
         * 获取接收方账户
         */
        getToAccount() {
            if (!this.data.conversation || !this.data.conversation.conversationID) {
                return '';
            }
            switch (this.data.conversation.type) {
                case 'C2C':
                    return this.data.conversation.conversationID.replace('C2C', '');
                case 'GROUP':
                    return this.data.conversation.conversationID.replace('GROUP', '');
                default:
                    return this.data.conversation.conversationID;
            }
        },
        /**
         * 发送 TIM 消息
         * @param {*} message 
         */
        $sendTIMMessage(message) {
            // 向页面传递数据
            this.triggerEvent('sendMessage', {
                message,
            });
            wx.$TUIKit.sendMessage(message, {
                offlinePushInfo: {
                    disablePush: true,
                },
            }).then(res => {
                // 一项监听服务，本项目无引入
                // if (this.data.firstSendMessage) {
                //     wx.aegis.reportEvent({
                //         name: 'sendMessage',
                //         ext1: 'sendMessage-success',
                //         ext2: 'imTuikitExternal',
                //         ext3: app.globalData.SDKAppID
                //     });
                // }
            }).catch(err => {
                logger.log(`| TUI-chat | message-input | sendMessageError: ${err.code} `);
                this.triggerEvent('showMessageErrorImage', {
                    showErrorImageFlag: err.code,
                    message,
                });
            });
            this.setData({
                displayFlag: '',
            });
        },
    }
})
