// components/agreement/agreement.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        // 弹窗的显示
        show: {
            type: Boolean,
            value: false
        },
        height: {
            type: String,
            value: "50%"
        }
    },
    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        close() {
            this.setData({
                show: false
            });
        }
    }
})
