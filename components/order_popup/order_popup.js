// components/data_select/data_select.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        // 弹窗的显示
        show: {
            type: Boolean,
            value: false
        },
        title: {
            type: String,
            value: '订单弹窗'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        _index: 1
    },

    /**
     * 组件的方法列表
     */
    methods: {
        close() {
            this.setData({
                show: false
            });
        }
    }
})
