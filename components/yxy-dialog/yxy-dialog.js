Component({
    /**
     * 组件的属性列表
     */
    properties: {
        show: {
            type: Boolean,
            value: false
        },
        image: {
            type: String,
            value: 'https://s3.bmp.ovh/imgs/2022/06/12/2ae2544f5b3b4fa5.png'
        },
        content: {
            type: String,
            value: '一则提示的主要内容'
        },
        tips: {
            type: String,
            value: '请耐心等候'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        click: true
    },
    /**
     * 组件的方法列表
     */
    methods: {
        close() {
            this.setData({
                click: false
            });
            setTimeout(() => {
                this.setData({
                    show: false,
                    click: true
                });
            }, 500);
        }
    }
})