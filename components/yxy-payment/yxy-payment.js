// components/payment/payment.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        /**
         * 控制组件的显隐
         */
        show: {
            type: Boolean,
            value: false
        },
        /**
         * 控制组件的聚焦状态
         */
        focus: {
            type: Boolean,
            value: false
        },
        /**
         * 商家图标
         */
        imgUrl: {
            type: String,
            value: '/static/images/logo.png'
        },
        /**
         * 支付金额
         */
        money: {
            type: Number,
            value: 0
        },
        /**
         * 已输入密码
         */
        value: {
            type: String,
            value: ''
        },
        /**
         * 已输入密码长度
         */
        passLength: {
            type: Number,
            value: 0
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        click: true
    },

    lifetimes: {
        ready() {
            this.setData({
                focus: true
            });
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        /**
         * 输入事件
         * @param {*} e 
         */
        inputPwd(e) {
            this.setData({
                value: e.detail.value,
                passLength: e.detail.cursor
            });
            if(this.data.passLength >= 6) {
                // 当 6 位密码输入完毕，通知页面，并将值传回
                this.triggerEvent('payoff', {value: this.data.value, length: this.data.passLength});
            }
        },
        /**
         * 点击密码输入框聚焦
         */
        handleFocus() {
            this.setData({
                focus: true
            });
        },
        /**
         * 获得焦点时
         */
        handleUseFocus() {
            this.setData({
                focus: true
            });
        },
        /**
         * 失去焦点时
         */
        handleUseBlur() {
            this.setData({
                focus: false
            });
        },
        /**
         * 关闭输入框
         */
        close() {
            this.setData({
                click: false
            });
            setTimeout(() => {
                this.setData({
                    click: true,
                    show: false
                });
                this.triggerEvent('cancelpay', this.data.show);
            }, 500);
        },
        /**
         * 关闭输入框
         */
        closePay() {
            this.setData({
                click: false
            });
            setTimeout(() => {
                this.setData({
                    click: true,
                    show: false
                });
            }, 500);
        }
    }
})
