// components/yxy-selector/yxy-selector.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        show: {
            type: Boolean,
            value: false
        },
        /**
         * 选择器标题
         */
        title: {
            type: String,
            value: '退款原因'
        },
        /**
         * 原因列表
         */
        reasonList: {
            type: Array,
            value: [
                '商品急需',
                '商品信息拍错了',
                '地址/电话信息填错了',
                '协商一致退货退款',
                '对商品不满意',
                '未按约定时间交付',
                '其它原因(可在补充描述里详细说明)'
            ]
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        click: true,
        radioText: ''
    },

    /**
     * 组件的方法列表
     */
    methods: {
        /**
         * 选择发生改变
         * @param {*} e 
         */
        changeRadio(e) {
            this.data.radioText = e.detail.value;
        },
        /**
         * 关闭
         */
        close() {
            this.setData({
                click: false
            });
            setTimeout(() => {
                this.setData({
                    show: false,
                    click: true
                });
                this.triggerEvent('radioSelect', this.data.radioText);
            }, 500);
        }
    }
})
