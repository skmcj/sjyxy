// subpkg/afterSale/afterSale.js
const db = wx.cloud.database();
const _ = db.command;

Page({

    /**
     * 页面的初始数据
     */
    data: {
        orderId: '',     // 订单ID
        isShow: false,
        detailValue: '', // 补充描述内容
        reasonText: '',  // 所选原因
        orderMess: {}
    },
    /**
     * 选择退款原因
     */
    onToSelect() {
        this.setData({
            isShow: true
        });
    },
    /**
     * 记录选择原因
     * @param {*} e 
     */
    selectRadioText(e) {
        this.setData({
            reasonText: e.detail
        });
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if(options.id) {
            this.setData({
                orderId: options.id
            });
            this.getOrderMess(options.id);
        }
    },
    /**
     * 获取订单信息
     * @param {String} id 
     */
    getOrderMess(id) {
        db.collection('idlemallOrder').doc(id).get()
          .then(res => {
              console.log('get order success ==>', res);
              this.setData({
                  orderMess: res.data
              });
          }, err => {
              console.log('get order error ==>', err);
          });
    },
    /**
     * 提交退款原因
     * @param {*} e 
     */
    onToSubmit() {
        if(this.data.reasonText === '') {
            this.$showMsg('请选择退款原因');
        }else {
            // 修改订单状态为：5-退款中
            db.collection('idlemallOrder').doc(this.data.orderId).update({
                data: {
                    orderStatus: 5,
                    refundReason: {
                        reasonText: this.data.reasonText,
                        reasonDesc: this.data.detailValue
                    }
                }
            }).then(res => {
                // console.log('update order status success ==>', res);
                // 添加消息
                this.addGoodsMess(this.data.orderId, this.data.orderMess.goodsMess.goodsData.goodsName, 5);
                // 修改成功，转到查看订单页
                wx.redirectTo({
                    url: '/pages/view_order/view_order?titId=2',
                });
            }, err => {
                console.log('update order status error ==>', err);
            });
        }  
    },
    /**
     * 添加消息
     * @param {*} orderId 
     * @param {*} goodsName 
     * @param {*} state 
     */
    addGoodsMess(orderId, goodsName, state) {
        // 添加消息
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'addGoodsMess',
                reqData: {
                    orderId: orderId,
                    goodsName: goodsName,
                    state: state
                }
            }
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    /**
     * 选择原因事件（弹出框）
     */
    chooseBecause: function () {
        this.setData({
            hiddenFlag: false
        })
    },
    /**
     * 关闭售后原因按钮
     */
    onChangeHidden: function () {
        this.setData({
            hiddenFlag: true
        })
    },
    /**
     * 补充描述表单事件
     */
    changeText(e) {
        this.setData({
            detailValue: e.detail.value
        });
    },
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    }
})