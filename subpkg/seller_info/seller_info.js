// subpkg/seller_info/seller_info.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        goodsList: [],
        hasGoods: false,
        pageNum: 1,
        pageSize: 6,
        id: '',
        userInfo: {},
        userID: '',
        total: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if(options) {
            this.setData({
                id: options.id
            });
            this.getUserInfo();
            this.getSellerGoodsList(1);
        }
    },
    /**
     * 跳转到商品详情页
     * @param {*} e 
     */
    onToGoodsDetail(e) {
        //获取点击的商品的index
        let index = e.currentTarget.dataset.goodsindex
        let pageNum = e.currentTarget.dataset.pagenum;
        // console.log('goods detail ==>', this.data.goodsList[pageNum][index]);
        // 通过索引找到商品的_id
        let id = this.data.goodsList[pageNum][index].id;
        wx.navigateTo({
            url: '../goodsDetail/goodsDetail?id=' + id,
        });
    },
    /**
     * 获取用户信息
     */
    getUserInfo() {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getUserInfoByGoodsId',
                reqData: this.data.id
            }
        }).then(res => {
            // console.log('get userInfo by id success ==>', res);
            this.setData({
                userInfo: res.result.resData.userInfo,
                userID: res.result.resData.userID
            });
        }, err => {
            console.log('get userInfo by id error ==>', err);
        });
    },
    /**
     * 获取卖家出售商品
     * @param {*} pageNum 
     */
    getSellerGoodsList(pageNum) {
        wx.showLoading({
          title: '加载中',
        });
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getIdlemallSendByOpenId',
                reqData: {
                    goodsId: this.data.id,
                    pageSize: this.data.pageSize,
                    pageNum: pageNum
                }
            }
        }).then(res => {
            // console.log('get goods by id success ==>', res);
            if(res.result.resData.data) {
                this.setData({
                    ['goodsList[' + pageNum + ']']: res.result.resData.data,
                    hasGoods: true,
                    total: res.result.resData.total
                });
            }
        }, err => {
            console.log('get goods by id error ==>', err);
        }).finally(() => {
            wx.hideLoading();
        });
    },
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        if(this.data.pageNum * this.data.pageSize < this.data.total) {
            this.setData({
                pageNum: this.data.pageNum + 1
            });
            this.getSellerGoodsList(this.data.pageNum);
        }else {
            this.$showMsg('没有更多数据了');
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    /**
     * 弹出提示消息
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    }
})