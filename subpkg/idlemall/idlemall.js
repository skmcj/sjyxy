// subpkg/idlemall/idlemall.js
//调用数据库
const db = wx.cloud.database()
//定义初始化数据
let totalData = 0
Page({

    /**
     * 页面的初始数据
     */
    data: {
        goodsList: [],     // 商品数据，一个二维数组
        // 定义分类导航数组
        categoryList: [
            {icon: 'yxyic-all', textName: '全部'},
            {icon: 'yxyic-book-2', textName: '书籍'},
            {icon: 'yxyic-rule', textName: '用品'},
            {icon: 'yxyic-bag', textName: '箱包'},
            {icon: 'yxyic-other-3', textName: '其它'}
        ],
        currentIndex: 0,   // 当前分类选择
        id: '',            // 定义id，用于存储商品_id
        type: '全部',      // type存储查询数据库中商品类型
        hiddenFlag: false,     // true为商品列表显示 false为搜索无果显示
        pageSize: 6,       // 每页数量
        pageNum: 1,        // 当前页码
        total: 0
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getGoodsList(this.data.pageNum)
    },
    /**
     * 封装函数 获取全部数据库商品信息
     */
    getGoodsList(pageNum) {
        wx.showLoading({
          title: '加载中···',
        });
        // 判断查询类型
        let types = [];
        if(this.data.type === '全部') {
            types = ['书籍', '用品', '箱包', '其它'];
        }else {
            types = [this.data.type];
        }
        // 获取数据库idlemallSend数据
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getIdlemallSendByType',
                reqData: {
                    type: types,
                    pageSize: this.data.pageSize,
                    pageNum: pageNum
                }
            }
        }).then(res => {
            // console.log('getGoodsList success ==>', res);
            if(res.result.resData.data) {
                this.setData({
                    ['goodsList[' + (pageNum - 1) + ']']: res.result.resData.data,
                    hiddenFlag: true,
                    total: res.result.resData.total
                }); 
            }
        }, err => {
            console.log('getGoodsList error ==>', err);
        }).finally(() => {
            wx.hideLoading();
        });
    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.setData({
            hiddenFlag: false,
            goodsList: [],
            pageNum: 1
        });
        this.getGoodsList(this.data.pageNum);
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        if(this.data.pageNum * this.data.pageSize < this.data.total) {
            this.setData({
                pageNum: this.data.pageNum + 1
            });
            this.getGoodsList(this.data.pageNum);
        }else {
            this.$showMsg('没有更多数据了');
        }
        
    },
    /**
     * 查看商品详情事件 传参应该是传入商品的Id 最后详情页根据id查询数据库
     */
    onToGoodsDetail(e) {
        //获取点击的商品的index
        let index = e.currentTarget.dataset.goodsindex
        let pageNum = e.currentTarget.dataset.pagenum;
        // console.log('goods detail ==>', this.data.goodsList[pageNum][index]);
        // 通过索引找到商品的_id
        let id = this.data.goodsList[pageNum][index].id;
        wx.navigateTo({
            url: '../goodsDetail/goodsDetail?id=' + id,
        });
    },
    /**
     * 点击分类导航事件
     */
    onTurnKind(e) {
        let current = e.currentTarget.dataset.index
        let typeName = this.data.categoryList[current].textName
        this.setData({
            hiddenFlag: false,
            currentIndex: current,
            type: typeName,
            goodsList: [],
            pageNum: 1
        })
        this.getGoodsList(this.data.pageNum);
    },
    /**
     * 搜索
     */
    onSearch() {
        this.$showMsg('后续推出，尽请期待');
    },
    /**
     * 跳转到商品订单
     */
    onToOrder() {
        wx.navigateTo({
            url: '/pages/view_order/view_order?titId=2',
          }).then(res => {
              // 跳转成功
              // console.log('order_success to orderList success', res);
          }, err => {
              // 跳转失败
              console.log('order_success to orderList fail', err);
          });
    },
    /**
     * 跳转到消息
     */
    onToGoodsMess() {
        wx.switchTab({
          url: '/pages/mess/mess',
        });
    },
    /**
     * 查看商家主页
     */
    onLookSeller(e) {
        let id = e.currentTarget.dataset.id;
        console.log('seller id ==>', id);
        wx.navigateTo({
          url: '../seller_info/seller_info?id=' + id,
        });
    },
    /**
     * 进入商城管理事件
     */
    onTurnMallManage: function () {
        wx.navigateTo({
            url: "../mallManagement/mallManagement"
        })
    },
    /**
     * 发布商品事件
     */
    onTurnIssue: function () {
        wx.navigateTo({
            url: "../issue/issue"
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    /**
     * 弹出提示消息
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    }
})