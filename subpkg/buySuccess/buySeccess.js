// subpkg/buySuccess/buySeccess.js
//调用数据库
const db = wx.cloud.database()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        orderId: '', //存储传参过来的订单id
        goodsId: '', //存储传参过来的商品id
        orderMessage: {}, //空对象存储订单信息
        // goodsMessage: [], //空数组存储商品信息
        goodsImg: '', //存储商品图片
        goodsName: '', //存储商品名字
        buyerTotal: 0, //存储够买商品数量
        tips: '已完成支付'
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        //接收参数
        if (options) {
            this.setData({
                orderId: options.id, //订单id
                tips: options.type === '已支付' ? '已完成支付' : '订单创建成功'
            });
            // 查询订单
            this.getOrderMess(options.id);
        }
        // db.collection("idlemallOrder").doc(this.data.orderId).get()
        //   .then(res => {
        //     console.log('get success ==>', res);
        //     this.setData({
        //         orderMessage: res.data,
        //         buyerTotal: this.data.buyerNum
        //     })
        //     this.setData({
                
        //     })
        // }, err => {
        //     console.log('get error ==>', err);
        // });
    },
    /**
     * 根据 ID 获取用户信息
     * @param {*} id 
     */
    getOrderMess(id) {
        db.collection('idlemallOrder').doc(id).get()
          .then(res => {
              console.log('get order success ==>', res);
              res.data.createTime = this.getTimeFormat(res.data.createTime);
              this.setData({
                  orderMessage: res.data
              });
              console.log('data ==>', this.data.orderMessage);
          }, err => {
              console.log('get order error ==>', err);
          });
    },
    /**
     * 查看订单
     */
    onToOrderView() {
        wx.redirectTo({
          url: '/pages/view_order/view_order?titId=2',
        });
    },
    /**
     * 获取时间格式
     * yyyy/MM/DD HH:mm:ss
     */
    getTimeFormat(dateStr) {
        let date = new Date(dateStr);
        let fDateStr = date.getFullYear() + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2);
        let fTimeStr = ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
        console.log('dateStr ==>', dateStr, fDateStr + ' ' + fTimeStr);
        return fDateStr + ' ' + fTimeStr;
    },
    /**
     * 查询并更新商品信息
     */
    // checkGoods: function () {
    //     //查询商品信息
    //     db.collection("idlemallSend").where({
    //         _id: this.data.goodsId
    //     }).get({
    //         success: (res) => {
    //             this.setData({
    //                 goodsMessage: res.data //空数组存储商品信息
    //             })
    //             //获取商品图片和名字
    //             this.setData({
    //                 goodsName: this.data.goodsMessage[0].goodsData.goodsName,
    //                 goodsImg: this.data.goodsMessage[0].goodsImg[0]
    //             })
    //         }
    //     })
    // },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})