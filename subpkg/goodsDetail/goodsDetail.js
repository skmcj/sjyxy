// subpkg/goodsDetail/goodsDetail.js
//调用数据库
const app = getApp();
const db = wx.cloud.database()

Page({
    /**
     * 页面的初始数据
     */
    data: {
        goodsId: '', //存放传参过来的goods id
        goodsMessage: [], //存放数据库获取的所有商品信息
        scrollHeight: 0,
        indicatorDots: true, //图片轮播图指示点
        autoplay: true, //图片轮播图是否自动播放
        interval: 3500, //自动切换时间
        duration: 1000, //滑过动画时长
        circular: true, //是否衔接播放
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getScrollHeight();
        if(options) {
            let id = options.id;
            this.setData({
                goodsId: id
            });
            //获取数据库
            wx.cloud.callFunction({
                name: 'databaseApi',
                data: {
                    type: 'getIdlemallById',
                    reqData: id
                }
            }).then(res => {
                // console.log('get goods by id success ==>', res);
                this.setData({
                    goodsMessage: res.result.resData
                });
            }, err => {
                console.log('get goods by id error ==>', err);
            });
        }
    },
    /**
     * 查看卖家
     */
    onLook() {
        wx.navigateTo({
          url: '../seller_info/seller_info?id=' + this.data.goodsId,
        });
    },
    /**
     * 联系卖家
     */
    onToContact() {
        const payloadData = {
            conversationID: `C2C${this.data.goodsMessage.userID}`,
        };
        wx.navigateTo({
            url: `/pages/chat/chat?conversationInfomation=${JSON.stringify(payloadData)}`,
        });
    },
    /**
     * 设置滚动视图的高
     */
    getScrollHeight() {
        this.setData({
            scrollHeight: wx.getSystemInfoSync().windowHeight - 80
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {},

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    /**
     * 跳转到商品购买事件
     */
    onGoodsBuy() {
        let myUserID = app.globalData.userID;
        let sellerUserID = this.data.goodsMessage.userID;
        // console.log('userid ==>', myUserID, sellerUserID);
        if(myUserID !== sellerUserID) {
            wx.navigateTo({
                url: '../goodsBuy/goodsBuy?id=' + this.data.goodsId,
            });
            wx.setStorageSync('idlemallID', this.data.goodsId);
        }else {
            this.$showMsg('您不能购买自己发布的商品');
        }
        
    },
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    }
})