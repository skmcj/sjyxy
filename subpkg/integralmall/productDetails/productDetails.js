// pages/integralmall/productDetails/productDetails.js
const db = wx.cloud.database()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        shoppingList: {},
        scrollHeight: 0
    },

    /**
     * 商品详情
     * @param {*} id 
     */
    getProduct(id) {
        db.collection('shoppingList').doc(id).get()
          .then(res => {
              console.log('shop success ==>', res);
              this.setData({
                  shoppingList: res.data
              });
          }, err => {
              console.log('shop error ==>', err);
          });
    },
    /**
     * 获取滚动视图的高
     */
    getScrollHeight() {
        this.setData({
            scrollHeight: wx.getSystemInfoSync().windowHeight - 60
        });
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if(options.id) {
            this.getProduct(options.id);
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getScrollHeight();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    //页面跳转
    toOrder(e) {
        this.$showMsg('积分商城正在升级维护中，敬请期待');
        // wx.navigateTo({
        //     url: '../order/order',
        // });
    },
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
            title: msg,
            icon: type
        });
    }
})