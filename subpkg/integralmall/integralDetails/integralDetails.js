// pages/integralmall/integralDetails/integralDetails.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        _cateIndex: 0,
        scrollHeight: 600,
        integral: 0,
        reduceIntegral: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },
    /**
     * 获取积分
     */
    getIntegral() {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getUserAccount'
            }
        }).then(res => {
            // console.log('getUserAccount success ==>', res);
            this.setData({
                integral: res.result.resData.integral,            // 积分
                reduceIntegral: res.result.resData.reduceIntegral // 消耗积分
            });
        }, err => {
            console.log('getUserAccount error ==>', err);
        });
    },
    /**
     * 选择分类
     */
    onSelectCate(e) {
        let index = e.currentTarget.dataset.index;
        // console.log('index ==>', index);
        this.setData({
            _cateIndex: index
        });
    },
    /**
     * 获取滚动视图的高
     */
    getScrollHeight() {
        this.setData({
            scrollHeight: wx.getSystemInfoSync().windowHeight - 279
        });
    },
    /**
     * 显示提示
     */
    onShowTip() {
        this.$showMsg('后续推出，敬请期待');
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getScrollHeight();
        this.getIntegral();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
            title: msg,
            icon: type
        });
    }
})