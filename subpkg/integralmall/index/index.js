// pages/integralmall/recommend/recommend.js
const db = wx.cloud.database();
const _ = db.command;

Page({

    /**
     * 页面的初始数据
     */
    data: {
        cateList: ['推荐', '文具', '饰品'], // 分类列表
        _cateIndex: 0, // 当前选择
        goodsList: [],
        integral: 0,
        pageSize: 6,
        pageNum: 1,
        total: 0,
        isLoading: false
    },
    onLoad(options) {},
    /**
     * 获取商品
     * @param {Array} type 
     */
    getShopList(type, pageNum) {
        wx.showLoading({
          title: '加载中',
        })
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getIntegralList',
                reqData: {
                    type: type,
                    pageSize: this.data.pageSize,
                    pageNum: pageNum
                }
            }
        }).then(res => {
            // console.log('getList success ==>', res);
            if(res.result.resData.data) {
                this.setData({
                    ['goodsList[' + (pageNum - 1) + ']']: res.result.resData.data,
                    total: res.result.resData.total
                });
            }
        }, err => {
            console.log('getList error ==>', err);
        }).finally(() => {
            this.data.isLoading = false;
            wx.hideLoading();
        });
    },
    /**
     * 获取积分
     */
    getIntegral() {
        wx.cloud.callFunction({
            name: 'databaseApi',
            data: {
                type: 'getUserAccount'
            }
        }).then(res => {
            // console.log('getUserAccount success ==>', res);
            this.setData({
                integral: res.result.resData.integral            // 积分
            });
        }, err => {
            console.log('getUserAccount error ==>', err);
        });
    },
    /**
     * 跳转到积分明细
     */
    onToDetailed() {
        wx.navigateTo({
            url: '/subpkg/integralmall/integralDetails/integralDetails',
        });
    },
    /**
     * 跳转到兑换记录
     */
    onToRecord() {
        wx.navigateTo({
            url: '/subpkg/integralmall/exchangeHistroy/exchangeHistroy',
        });
    },
    /**
     * 选择分类
     * @param {*} e 
     */
    onSelectCate(e) {
        let index = e.currentTarget.dataset.index;
        // console.log('index ==>', index);
        this.setData({
            _cateIndex: index,
            goodsList: [],
            pageNum: 1
        });
        if(index === 0) {
            this.getShopList(['文具', '饰品'], 1);
        }else if(index === 1) {
            this.getShopList(['文具'], 1);
        }else {
            this.getShopList(['饰品'], 1);
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getIntegral();
        this.getShopList(['文具', '饰品'], 1);
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        if(!this.data.isLoading && this.data.pageNum * this.data.pageSize < this.data.total) {
            let type = [];
            if(this.data._cateIndex === 0) {
                type = ['文具', '饰品'];
            }else if(this.data._cateIndex === 1) {
                type = ['文具'];
            }else {
                type = ['饰品'];
            }
            this.data.isLoading = true;
            this.data.pageNum += 1;
            this.getShopList(type, this.data.pageNum);
        }else {
            this.$showMsg('没有更多商品了');
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    //页面跳转
    toIntegralDetail(e) {
        wx.navigateTo({
            url: '../integralDetails/integralDetails',
        })
    },

    toExchangeHistroy(e) {
        wx.navigateTo({
            url: '../exchangeHistroy/exchangeHistroy',
        })
    },
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
          title: msg,
          icon: type
        });
    }
})