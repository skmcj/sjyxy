// pages/integralmall/exchangeHistroy/exchangeHistroy.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        startDate: '2019-01',
        endDate: '2022-12',
        date: '2022-06'
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.getNowDate();
    },
    /**
     * 日期选择
     * @param {*} e 
     */
    changeDate(e) {
        // console.log('date ==>', e);
        this.setData({
            date: e.detail.value
        });
    },
    /**
     * 获取当前日期
     */
    getNowDate() {
        let date = new Date();
        let dateStr = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2);
        this.setData({
            endDate: dateStr,
            date: dateStr
        });
    },
    /**
     * 显示提示
     */
    onShowTip() {
        this.$showMsg('后续推出，敬请期待');
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    },
    toOrderDetail(e){
        wx.navigateTo({
          url: '../orderDetails/orderDetails',
        })
    },
    /**
     * 显示提示
     * @param {*} msg 
     * @param {*} type 
     */
    $showMsg(msg, type = 'none') {
        wx.showToast({
            title: msg,
            icon: type
        });
    }
})