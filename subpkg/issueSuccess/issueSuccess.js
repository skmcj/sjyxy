// subpkg/issueSuccess/issueSuccess.js
//调用数据库
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsMessage: [], //存放发布商品信息
    goodsId: '',      //存放传参的商品id
    textContect: '',  //判断是商品发布还是修改
    hasGoods: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    //接收参数
    let flag = "发布"
    if(options) {
        if(options.flag === '1') {
            flag = '修改';
        }
        wx.setNavigationBarTitle({
            title: flag + '商品'
        });
        this.setData({
            goodsId: options.id,
            textContect: flag
        });
        //获取数据库
        if(options.id) {
            db.collection("idlemallSend").doc(options.id)
                .get().then(res => {
                    // console.log('get goods success ==>', res);
                    this.setData({
                        goodsMessage: res.data,
                        hasGoods: true
                    });
                }, err => {
                    console.log('get goods error ==>', err);
                });
        }
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  /**
   * 点击查看详情事件
   */
  lookDetail: function () {
    wx.redirectTo({
      url: '../goodsDetail/goodsDetail?id=' + this.data.goodsId,
    })
  }
})