# 韶+益校园项目

## 项目基础结构

- `cloudFunctions` — 项目的云函数文件，部署于云端，后期可删
  - `databaseApi` — 用于数据库操作的云函数
- `components` — 自定义组件文件夹

  - `base` — 组件基础依赖包

  - `agreement` — 登录页底部的弹出协议
  - `order_popup` — 回收订单页的底部弹窗
  - `popupbox` — 中部弹窗
  - `yxy-chat` — 会话相关组件
    - `mess-elements` — 消息元素
      - `emoji` — 表情组件
      - `mess-audio` — 语音消息
      - `mess-image` — 图片消息
      - `mess-text` — 文本消息
      - `mess-video` — 视频消息
    - `mess-input` — 输入框组件
    - `mess-list` — 聊天列表组件
  - `yxy-conversation` — 会话项组件
  - `yxy-dialog` — 底部提示弹框
  - `yxy-payment` — 模拟微信支付框
  - `yxy-selector` — 弹出选择器
- `static` — 存放项目的静态文件
  
  - `images` — 图片文件夹，存放项目图片，如：`tabBar`的图标
  - `font` — 存放项目的字体文件
  - `plugin` — 腾讯`IM`依赖包
- `pages` — 页面文件夹
  - `index` — 首页
  - `recycle` — 一键回收页面
  - `mess` — 消息页面
  - `my` — 我的页面
  - `login` — 登录页
  - `address_administration` — 地址管理页面
  - `new_address` — 添加地址页面
  - `place_order` — 回收下单页面
  - `order_successful` — 回收下单成功页面
  - `chat` — 会话页
  - `helpCenter` — 帮助中心
  - `order_detail` — 订单详情页
  - `order_notice` — 订单消息页
  - `view_order` — 查看订单页
  - `account` — 账户统计模块
  - `idea` — 意见反馈
  - `idea_history`  — 历史反馈页
  - `personal_info` — 个人信息页
  - `info_edit` — 个人信息编辑页
- `subpkg` — 分包
  - `integralmall` — 积分商城
    - `index` — 积分商城首页
    - `integralDetails` — 积分明细页
    - `exchangeHistroy` — 兑换记录页
    - `productDetails` — 商品详情页
  - `idlemall` — 闲置商城
  - `afterSale` — 退款页面
  - `buySuccess` — 下单后状态页[支付 | 未支付]
  - `goodsBuy` — 下单页
  - `goodsDetail` — 商品详情页
  - `issue` — 商品发布 | 编辑页
  - `issueSuccess` — 发布成功页
  - `mallManagement` — 闲置商城管理页
  - `seller_info` — 卖家信息页
  - `search` — 搜索页面
- `utils` — 工具文件夹

  - `api.js` — 腾讯`IM`的一些`Api`
  - `common-utils.js` — 腾讯`IM`的依赖文件
  - `console.js` — 腾讯`IM`的依赖文件
  - `logger.js` — 腾讯`IM`的依赖文件
  - `token.js` — 腾讯`IM`的依赖文件
  - `tools.wxs` — 主要用于页面的计算文件

  - `utils.js` — 主要的工具文件
- `··· ···`

## 项目安装

- **环境准备**
  - 小程序账号：`APPID`
  - 小程序云环境：`环境ID`
  - 腾讯IM服务[应用]([腾讯云-控制台 (tencent.com)](https://console.cloud.tencent.com/im))：`SDKAPPID`、`SECRETKEY`(密钥)
  
- **代码安装**

  - 本地修改

    - `/app.js` — 在该文件配置自己的云环境ID

      - ```js
        onLaunch() {
            wx.removeStorageSync('recycleAddress');
            this.initGlobalData();
            wx.cloud.init({
                env: '自己的小程序云环境ID',
                traceUser: true
            });
            // 初始化IM
            this.initTIM();
        },
        ```

    - `/project.config.json` — 修改小程序的APPID

      - ```js
        "appid": "自己的小程序APPID",
        ```

    - `/static/plugin/root-data.js` — 将该文件里的`SDKAPPID`改为自己的

  - 云函数(`databaseApi`)修改

    - ```js
      /** index.js */
      // 将以下SDKAPPID和SECRETKEY密钥配置为自己的腾讯IM应用的
      // 腾讯云即时通讯IM
      const _SDKAPPID = 0;
      // 应用秘钥
      const _SECRETKEY = '';
      ```

    - 修改好后，上传并部署
    
  - 云数据库修改
  
    - 需在小程序云开发数据库添加以下集合(表)
    - `users`表 — 用于存放用户信息
    - `address`表 — 用于存放用户添加地址
    - `order_mess`表 — 用于记录用户订单消息
    - `recycle_order`表 — 用于记录用户的回收订单
    - `idlemallSend`表 — 用于记录用户发布的闲置商品
    - `idlemallOrder`表 — 用于记录用户闲置商品订单
    - `feedback`表 — 用户记录用户反馈信息
    - `shoppingList`表 — 用户记录积分商品信息
    
  - [数据库表详情](./database.md)

