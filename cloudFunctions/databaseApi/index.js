// 云函数入口文件
const cloud = require('wx-server-sdk');
const TLSSigAPIv2 = require('tls-sig-api-v2');

cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database();
const _ = db.command;

// 以下秘钥需配置为自己的
// 腾讯云即时通讯IM
const _SDKAPPID = 0;
// 应用秘钥
const _SECRETKEY = '';

// 云函数入口函数
/**
 * @author skmcj
 * @version 1.6.3
 * @description 将时区设置为 Asia/Shanghai
 * @param {*} event 
 * @param {*} context 
 */
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext();
    let resData = {};
    
    switch(event.type) {
        case 'login':
            // 登录 | 注册
            resData = await login(wxContext.OPENID, event.reqData);
            break;
        case 'updateUser':
            // 修改用户数据
            resData = await updateUser(wxContext.OPENID, event.reqData);
            break;
        case 'setUser':
            // 新增用户
            resData = await setUser(wxContext.OPENID, event.reqData);
            break;
        case  'hasUser':
            // 判断用户是否存在
            resData = await hasUser(wxContext.OPENID);
            break;
        case 'saveAddress':
            // 新增地址
            resData = await saveAddress(wxContext.OPENID, event.reqData);
            break;
        case 'getAddress':
            // 获取用户地址
            resData = await getAddress(wxContext.OPENID);
            break;
        case 'updateAddress':
            // 保存用户修改
            resData = await updateAddress(wxContext.OPENID, event.reqData);
            break;
        case 'addRecycleOrder':
            // 添加一个回收订单
            resData = await addRecycleOrder(wxContext.OPENID, event.reqData);
            break;
        case 'getMessList':
            // 获取回收订单消息列表
            resData = await getMessList(wxContext.OPENID, event.reqData.timeStamp, event.reqData.type);
            break;
        case 'readMess':
            // 将订单消息标记为已读
            resData = await readMess(wxContext.OPENID, event.reqData);
            break;
        case 'comRecycleMess':
            // 点击确认完成订单
            resData = await comRecycleMess(wxContext.OPENID, event.reqData);
            break;
        case 'getRecycleOrderList':
            // 获取回收订单列表
            resData = await getRecycleOrderList(wxContext.OPENID, event.reqData.state, event.reqData.pageSize, event.reqData.pageNum);
            break;
        case 'getIdlemallSendByType':
            // 根据商品类型获取商品
            resData = await getIdlemallSendByType(event.reqData.type, event.reqData.pageSize, event.reqData.pageNum);
            break;
        case 'getIdlemallSendByOpenId':
            // 根据用户openid获取商品
            resData = await getIdlemallSendByOpenId(event.reqData.goodsId, event.reqData.pageSize, event.reqData.pageNum);
            break;
        case 'getIdlemallById':
            // 根据商品ID获取商品信息
            resData = await getIdlemallById(event.reqData);
            break;
        case 'getUserInfoByGoodsId':
            // 通过商品ID获取用户信息
            resData = await getUserInfoByGoodsId(event.reqData);
            break;
        case 'getIdlemallOfUser':
            // 获取用户发布的商品
            resData = await getIdlemallOfUser(wxContext.OPENID, event.reqData.pageSize, event.reqData.pageNum);
            break;
        case 'addIdlemallOrder':
            // 添加商品订单
            resData = await addIdlemallOrder(wxContext.OPENID, event.reqData);
            break;
        case 'getIdlemallOrderList':
            // 获取用户商品订单
            resData = await getIdlemallOrderList(wxContext.OPENID, event.reqData.state, event.reqData.pageSize, event.reqData.pageNum);
            break;
        case 'cancelBuy':
            // 商品订单：取消购买
            resData = await cancelBuy(event.reqData.itemNo, event.reqData.amount, event.reqData.orderId);
            break;
        case 'getOrderByUser':
            // 获取用户闲置商品订单
            resData = await getOrderByUser(wxContext.OPENID, event.reqData.pageSize, event.reqData.pageNum);
            break;
        case 'getBuyIdByOrderId':
            // 根据订单ID获取用户的userID
            resData = await getBuyIdByOrderId(event.reqData);
            break;
        case 'addGoodsMess':
            // 添加闲置商品消息
            resData = await addGoodsMess(wxContext.OPENID, event.reqData.orderId, event.reqData.goodsName, event.reqData.state);
            break;
        case 'getFeedback':
            // 分页获取用户历史意见反馈
            resData = await getFeedback(wxContext.OPENID, event.reqData.pageSize, event.reqData.pageNum);
            break;
        case 'updateUserInfo':
            // 修改用户信息
            resData = await updateUserInfo(wxContext.OPENID, event.reqData.type, event.reqData.value);
            break;
        case 'updateUserAccount':
            // 修改用户统计值
            resData = await updateUserAccount(wxContext.OPENID, event.reqData.type, event.reqData.incNum);
            break;
        case 'getUserAccount':
            // 获取用户统计模块值
            resData = await getUserAccount(wxContext.OPENID);
            break;
        case 'updateAccountByIdleOrder':
            // 闲置订单完成时，根据闲置商品ID修改用户统计
            resData = await updateAccountByIdleOrder(wxContext.OPENID, event.reqData);
            break;
        case 'getIntegralList':
            // 获取积分商品
            resData = await getIntegralList(event.reqData.type, event.reqData.pageSize, event.reqData.pageNum);
            break;
        case 'updateOrderStatus':
            resData = await updateOrderStatus(event.reqData.orderId, event.reqData.status);
            break;
    }
    

    return {
        resData
    }
}
/* <===== 登录 | 注册 操作 =====> */
/**
 * 登录操作
 * @param {*} openId 
 * @param {*} reqData 
 */
async function login(openId, reqData) {
    let data = {};
    // 查询用户记录
    const isU = await db.collection('users').where({
                            _openid: openId
                        }).get();
    if(isU.data.length > 0) {
        // 已有用户，直接返回数据
        data.flag = true;
        data.userInfo = isU.data[0].userInfo;
        data.userID = isU.data[0].userID;
        data.isTimUpdate = isU.data[0].isTimUpdate;
        data.userSig = getUserSig(isU.data[0].userID);
    }else {
        // 未有用户，记录数据，并返回数据
        let userIDStr = getUserID();
        const res = await db.collection('users').doc(openId)
                            .set({
                                data: {
                                    _id_: openId,
                                    _openid: openId,
                                    account: {
                                        income: 0,               // 总收入
                                        pay: 0,                  // 总支出
                                        orderCount: 0,           // 完成订单量
                                        recycleProfit: 0,        // 回收收益
                                        idleProfit: 0,           // 闲置收益
                                        integral: 0,             // 积分
                                        reduceIntegral: 0        // 累计消耗积分
                                    },
                                    userInfo: reqData,
                                    userID: userIDStr,
                                    isTimUpdate: false
                                }
                            });
        data.flag = false;
        data.userInfo = reqData;
        data.userID = userIdStr;
        data.isTimUpdate = false;
        data.userSig = getUserSig(userIdStr);
    }
    return data;
}

/* <===== 用户表操作 =====> */
/**
 * 将用户信息保存到数据库，有则修改，无则新增
 * @param {String} openId 
 * @param {Object} reqData 
 */
async function setUser(openId, reqData) {
    let userIdStr = getUserID();
    const res = await db.collection('users').doc(openId)
                        .set({
                            data: {
                                _id_: openId,
                                _openid: openId,
                                userInfo: reqData,
                                userID: userIdStr
                            }
                        });
    return res;
}
/**
 * 修改用户数据
 * @param {String} openId 
 * @param {Object} reqData 
 */
async function updateUser(openId, reqData) {
    const res = await db.collection('users').doc(openId)
                        .update({
                            data: reqData
                        });
    return res;
}
/**
 * 获取指定用户信息
 * @param {String} openId 
 */
async function hasUser(openId) {
  let data = {};
  const res = await db.collection('users').where({
                      _id: openId
                    }).get();
  if(res.data.length > 0) {
    data.flag = true;
    data.userInfo = res.data[0].userInfo;
    data.userID = res.data[0].userID;
    data.userSig = getUserSig(res.data[0].userID);
  }else {
    data.flag = false;
    data.userInfo = null;
  }
  return data;
}
/**
 * 修改用户信息
 * @param {*} openId 
 * @param {*} type 
 * @param {*} value 
 */
async function updateUserInfo(openId, type, value) {
    const uRes = await db.collection('users').doc(openId).update({
        data: {
            userInfo: {
                [type]: value
            }
        }
    });
    return uRes;
}
/**
 * 修改用户统计模块
 * @param {String} openId 
 * @param {Array} type 
 * @param {Array} incNum 
 */
async function updateUserAccount(openId, type, incNum) {
    let typeList = {
        income: 0,              // 总收入
        pay: 0,                 // 总支出
        orderCount: 0,          // 完成订单量
        recycleProfit: 0,       // 回收收益
        idleProfit: 0,          // 闲置收益
        integral: 0             // 积分
    };
    for(let i = 0; i < type.length; i++) {
        typeList[type[i]] = incNum[i];
    }
    const aRes = await db.collection('users').doc(openId).update({
        data: {
            account: {
                income: _.inc(typeList.income),
                pay: _.inc(typeList.pay),
                orderCount: _.inc(typeList.orderCount),
                recycleProfit: _.inc(typeList.recycleProfit), 
                idleProfit: _.inc(typeList.idleProfit),
                integral: _.inc(typeList.integral)
            }
        }
    });
    return aRes;
}
/**
 * 根据商品ID修改统计模块
 * @param {*} openId 
 * @param {*} orderId 
 */
async function updateAccountByIdleOrder(openId, orderId) {
    let rData = {};
    const iRes = await db.collection('idlemallOrder').doc(orderId).get();
    let sellerUserID = iRes.data.goodsMess.userID;
    let payment = parseFloat(iRes.data.payment);
    const oRes = await db.collection('users').where({
        userID: sellerUserID
    }).get();
    let sellerOpenId = oRes.data[0]._openid;
    // buyer => 总支出、订单量
    rData.buyer = await db.collection('users').doc(openId).update({
        data: {
            account: {
                pay: _.inc(payment),
                orderCount: _.inc(1)
            }
        }
    });
    // seller => 总收入、闲置收入
    rData.seller = await db.collection('users').doc(sellerOpenId).update({
        data: {
            account: {
                income: _.inc(payment),
                idleProfit: _.inc(payment)
            }
        }
    });
    return rData;
}
/**
 * 获取用户的统计
 * @param {*} openId 
 */
async function getUserAccount(openId) {
    const uRes = await db.collection('users').doc(openId).get();
    return uRes.data.account;
}

/* <===== 地址表操作 =====> */
/**
 * 新增用户地址
 * @param {String} openId 用户的 openID
 * @param {Object} reqData 用户传入数据，为一个地址信息对象
 * @param {String} reqData.name 地址=>联系人
 * @param {String} reqData.sex 地址=>性别称呼
 * @param {String} reqData.phone 地址=>电话
 * @param {String} reqData.location 地址=>定位信息
 * @param {String} reqData.desc 地址=>详细地址
 * @param {Boolean} reqData.default 地址=>是否默认
 */
async function saveAddress(openId, reqData) {
    let oldAddress = [];
    const res1 = await db.collection('address').where({
                         _id: openId
                       }).get();
    if(res1.data.length > 0) {
        // 获取用户已有地址
        oldAddress = res1.data[0].address;
    }else {
        // 将第一个地址设为默认地址
        reqData.default = true;
    }
    const res2 = await db.collection('address').doc(openId)
                         .set({
                            data: {
                              _id_: openId,
                              _openid: openId,
                              address: [...oldAddress, reqData]
                            }
                          });
    return res2;
}
/**
 * 获取用户地址
 * @param {String} openId 
 */
async function getAddress(openId) {
    const res = await db.collection('address').doc(openId).get();
    return res.data.address;
}
/**
 * 修改用户的地址信息
 * @param {*} openId 
 * @param {*} reqData 
 */
async function updateAddress(openId, reqData) {
    const res = await db.collection('address').doc(openId)
                        .update({
                            data: {
                                address: reqData
                            }
                        });
    return res;
}

/* <===== 回收订单表操作 =====> */
/**
 * 添加回收订单
 * @param {*} openId 
 * @param {*} reqData 
 */
async function addRecycleOrder(openId, reqData) {
    const res = await db.collection('recycle_order').add({
                          data: {
                              _openid: openId,
                              orderForm: reqData
                          }
                      });
    addRecycleMess(openId, res._id, reqData);
    return res;
}
/**
 * 更新回收订单状态
 * @param {*} orderId 
 * @param {*} newState 
 */
async function updateRecycleOrderState(orderId, newState) {
    const res = await db.collection('recycle_order').doc(orderId)
                        .update({
                            data: {
                                orderForm: {
                                    state: newState
                                } 
                            }    
                        });
    return res;
}
/**
 * 获取回收订单
 * @param {*} openId 
 * @param {*} state 
 * @param {*} pageSize
 * @param {*} pageNum
 */
async function getRecycleOrderList(openId, state, pageSize, pageNum) {
    let rData = {};
    let begin = (pageNum - 1) * pageSize;
    let size = pageSize;
    const cRes = await db.collection('recycle_order').where({
        _openid: openId,
        orderForm: {
            state: _.in(state)
        }
    }).count();
    let total = cRes.total;
    rData.total = total;
    rData.data = false;
    if(begin < total) {
        const oRes = await db.collection('recycle_order').where({
            _openid: openId,
            orderForm: {
                state: _.in(state)
            }
        }).skip(begin).limit(size).get();
        rData.data = oRes.data;
    }
    
    return rData;
}

/* <===== 闲置商品订单表操作 =====> */
/* 订单状态：0-待付款，1-待发货，2-已发货，3-售后中，4-已完成，5-退款中，6-退款后 */
/**
 * 获取商品订单列表
 * @param {*} openId 
 * @param {Array} state 
 * @param {*} pageSize 
 * @param {*} pageNum 
 */
async function getIdlemallOrderList(openId, state, pageSize, pageNum) {
    let rData = {};
    let begin = (pageNum - 1) * pageSize;
    let size = pageSize;
    const cRes = await db.collection('idlemallOrder').where({
        _openid: openId,
        orderStatus: _.in(state)
    }).count();
    let total = cRes.total;
    rData.total = cRes.total;
    rData.data = false;
    if(begin < total) {
        const oRes = await db.collection('idlemallOrder').where({
            _openid: openId,
            orderStatus: _.in(state)
        }).skip(begin).limit(size).get();
        rData.data = oRes.data;
    }
    return rData;
}
/**
 * 获取用户发布商品生成的订单
 * @param {*} openId 
 * @param {*} pageSize 
 * @param {*} pageNum 
 */
async function getOrderByUser(openId, pageSize, pageNum) {
    let rData = {};
    let begin = (pageNum - 1) * pageSize;
    let size = pageSize;
    const uRes = await db.collection('users').doc(openId).get();
    let userID = uRes.data.userID;
    const cRes = await db.collection('idlemallOrder').where({
        orderStatus: _.in([1, 2, 3, 4, 5, 6]),
        goodsMess: {
            userID: userID
        }
    }).count();
    let total = cRes.total;
    rData.total = cRes.total;
    if(begin < total) {
        const oRes = await db.collection('idlemallOrder').where({
            orderStatus: _.in([1, 2, 3, 4, 5, 6]),
            goodsMess: {
                userID: userID
            }
        }).skip(begin).limit(size).get();
        rData.data = packageIdlemallOrderData(oRes.data);
    }else {
        rData.data = false;
    }
    
    return rData;
}
/**
 * 根据订单ID获取用户userID
 * @param {*} orderId 
 */
async function getBuyIdByOrderId(orderId) {
    const oRes = await db.collection('idlemallOrder').doc(orderId).get();
    let openId = oRes.data._openid;
    const uRes = await db.collection('users').doc(openId).get();
    let userID = uRes.data.userID;
    return userID;
}
/**
 * 修改订单状态
 * @param {*} orderId 
 * @param {*} status 
 */
async function updateOrderStatus(orderId, status) {
    const uRes = await db.collection('idlemallOrder').doc(orderId).update({
                            data: {
                                orderStatus: status
                            }
                        });
    return uRes;
}
/**
 * 包装数据
 * @param {*} data 
 */
function packageIdlemallOrderData(data) {
    let rData = [];
    for(let i = 0; i < data.length; i++) {
        let item = {};
        item.id = data[i]._id;
        item.address = data[i].address;
        item.amount = data[i].amount;
        item.createTime = data[i].createTime;
        item.goodsMess = data[i].goodsMess;
        item.itemNo = data[i].itemNo;
        item.orderCode = data[i].orderCode;
        item.orderDetail = data[i].orderDetail;
        item.orderStatus = data[i].orderStatus;
        item.payTime = data[i].payTime;
        item.payment = data[i].payment;
        rData.push(item);
    }
    return rData;
}
/* ------- 功能操作 ------- */
/**
 * 取消购买
 * @param {*} itemNo 
 * @param {*} amount 
 * @param {*} orderId 
 */
async function cancelBuy(itemNo, amount, orderId) {
    // 获取商品信息
    const gRes = await db.collection('idlemallSend').doc(itemNo).get();
    let goodsStatus = gRes.data.goodsStatus === '已下架' ? '已下架' : '出售中';
    let goodsNum = gRes.data.goodsData.goodsNum;
    // 修改商品库存
    const uRes = await db.collection('idlemallSend').doc(itemNo).update({
        data: {
            goodsData: {
                goodsNum: goodsNum + amount
            },
            goodsStatus: goodsStatus
        }
    });
    // 删除订单
    const dRes = await db.collection('idlemallOrder').doc(orderId).remove();
    return dRes;
}

/* <===== 订单消息表操作 =====> */
/**
 * 获取指定 id 的订单，有则返回数据，无则创建
 * @param {*} orderId 
 */
async function getOrderMess(orderId) {
    let data = {};
    const res = await db.collection('order_mess').where({
                            _id: orderId
                        }).get();
    if(res.data.length > 0) {
        data = res.data[0];
    }else {
        let addData = {};
        addData._id_ = orderId;
        addData._openid = orderId;
        addData.recycleMess = [];
        addData.shopMess = [];
        db.collection('order_mess').doc(orderId).set({
            data: addData
        });
        data = addData;
    }
    return data;
}
/**
 * 获取回收消息列表
 * @param {*} openId 
 * @param {*} timeStamp 
 */
async function getMessList(openId, timeStamp, type) {
    let rData = [];
    if(type === 'recycle') {
        let cRes = await checkedState(openId, timeStamp);
        const res = await db.collection('order_mess').doc(openId).get();
        res.data.recycleMess.sort((a, b) => {return b.createTimeStamp - a.createTimeStamp});
        rData = res.data.recycleMess;
    }else {
        const res = await db.collection('order_mess').doc(openId).get();
        rData = res.data.shopMess;
    }
    return rData;
}
async function readMess(openId, type) {
    let res = {};
    if(type === 'recycle') {
        res = await db.collection('order_mess').doc(openId).update({
            data: {
                'recycleMess.$[].isRead': true
            }
        }); 
    }else {
        res = await db.collection('order_mess').doc(openId).update({
            data: {
                'shopMess.$[].isRead': true
            }
        }); 
    }
    return res;

    
}
/**
 * 增加单条回收消息
 */
async function addRecycleMess(openId, orderId, reqData) {
    let rRes = await getOrderMess(openId);
    let addMess = {};
    let date = new Date();
    addMess.orderId = orderId;
    addMess.createTime = getTimeFormat1(date);
    addMess.orderAmount = reqData.amountStr;
    addMess.createTimeStamp = date.getTime();
    addMess.orderState = reqData.state;
    addMess.isRead = false;
    const res = await db.collection('order_mess').doc(openId).update({
        data: {
            recycleMess: [...rRes.recycleMess, addMess]
        }
    });
    return res;
}
/**
 * 批量增加回收订单消息
 * @param {*} openId 
 * @param {*} messList 
 */
async function pushRecycleMess(openId, messList) {
    let rRes = await getOrderMess(openId);
    const res = await db.collection('order_mess').doc(openId).update({
        data: {
            recycleMess: [...rRes.recycleMess, ...messList]
        }
    });
    return [messList, res];
}
/**
 * 点击完成回收订单
 * @param {*} openId 
 * @param {*} orderId 
 */
async function comRecycleMess(openId, orderId) {
    let rRes = await db.collection('recycle_order').doc(orderId).update({
        data: {
            orderForm: {
                state: 2
            }
        }
    });
    let uRes = await db.collection('recycle_order').doc(orderId).get();
    let res = await addRecycleMess(openId, orderId, uRes.data.orderForm);
    return res;
}
/**
 * 增加单条商品订单消息
 * @param {*} openId 
 * @param {*} orderId 
 * @param {*} reqData 订单状态
 */
async function addGoodsMess(openId, orderId, goodsName, state) {
    let oRes = await db.collection('idlemallOrder').doc(orderId).get();
    let buyerId = oRes.data._openid;
    let rRes = await getOrderMess(buyerId);
    let date = new Date();
    let addMess = {};
    addMess.orderId = orderId;
    addMess.createTime = (date.getFullYear() % 100) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2);
    addMess.goodsName = goodsName;
    addMess.createTimeStamp = date.getTime();
    addMess.orderState = state;
    addMess.orderAvatar = oRes.data.goodsMess.goodsImg[0];
    addMess.isRead = false;
    const res = await db.collection('order_mess').doc(buyerId).update({
        data: {
            shopMess: [addMess, ...rRes.shopMess]
        }
    });
    return res;
}

/* <===== 闲置商品表的操作 =====> */
/**
 * 添加商品订单
 * @param {String} openId 
 * @param {Object} data 
 */
async function addIdlemallOrder(openId, data) {
    // 向商品订单表添加订单数据
    const res = await db.collection('idlemallOrder').add({
        data: {
            _openid: openId,
            ...data
        }
    })
    // 向消息表添加订单消息
    // TODO
    return res;
}
/**
 * 查询用户账户下发布的商品
 * @param {*} goodsId 
 */
async function getUserInfoByGoodsId(goodsId) {
    let rData = {};
    const openIdRes = await db.collection('idlemallSend').doc(goodsId).get();
    const openId = openIdRes.data._openid;
    const userRes = await db.collection('users').doc(openId).get();
    rData.userInfo = userRes.data.userInfo;
    rData.userID = userRes.data.userID;
    return rData;
}
/**
 * 根据类型查询商品信息
 * @param {*} type 
 * @param {*} pageSize 
 * @param {*} pageNum 
 */
async function getIdlemallSendByType(type, pageSize, pageNum) {
    let res = {};
    // 当前所求页开始索引
    let begin = (pageNum - 1) * pageSize;
    // 每页大小
    let size = pageSize;
    // 获取查询结果
    const countRes = await db.collection('idlemallSend').where({
        goodsType: _.in(type)
    }).count();
    // 获取查询结果总数
    const total = countRes.total;
    if(begin < total) {
        const goodsRes = await db.collection('idlemallSend').where({
            goodsType: _.in(type)
        }).skip(begin).limit(size).get();
        res.data = await packIdlemallData(goodsRes.data);
    }else {
        res.data = false;
    }
    res.total = total;
    return res;
}
async function getIdlemallSendByOpenId(goodsId, pageSize, pageNum) {
    // 获取用户openId
    const openIdRes = await db.collection('idlemallSend').doc(goodsId).get();
    const openId = openIdRes.data._openid;
    // 返回数据
    let res = {};
    // 当前所求页开始索引
    let begin = (pageNum - 1) * pageSize;
    // 每页大小
    let size = pageSize;
    // 获取查询结果
    const countRes = await db.collection('idlemallSend').where({
        _openid: openId
    }).count();
    // 获取查询结果总数
    const total = countRes.total;
    if(begin < total) {
        const goodsRes = await db.collection('idlemallSend').where({
            _openid: openId
        }).skip(begin).limit(size).get();
        res.data = cleanIdlemallData(goodsRes.data);
    }else {
        res.data = false;
    }
    res.total = total;
    return res;
}
/**
 * 根据id获取商品信息
 * @param {*} id 
 */
async function getIdlemallById(id) {
    let res = {};
    const goodsRes = await db.collection('idlemallSend').doc(id).get();
    const userRes = await db.collection('users').doc(goodsRes.data._openid).get();
    res.userID = userRes.data.userID;
    res.userInfo = userRes.data.userInfo;
    res.goodsData = goodsRes.data.goodsData;
    res.goodsImg = goodsRes.data.goodsImg;
    res.goodsStatus = goodsRes.data.goodsStatus;
    res.goodsType = goodsRes.data.goodsType;
    return res;
}
/**
 * 获取当前用户发布的商品
 * @param {*} openId 
 */
async function getIdlemallOfUser(openId, pageSize, pageNum) {
    let res = {};
    let begin = (pageNum - 1) * pageSize;
    let size = pageSize;
    const cRes = await db.collection('idlemallSend').where({
                            _openid: openId
                        }).count();
    let total = cRes.total;
    res.total = total;
    if(begin < total) {
        const uRes = await db.collection('idlemallSend').where({
                            _openid: openId
                        }).skip(begin).limit(size).get();
        res.data = cleanIdlemallData(uRes.data);
    }else {
        res.data = false;
    }
    
    return res;
}

/**
 * 包装闲置商品数据
 * @param {*} data 
 */
async function packIdlemallData(data) {
    // 包装后的数据
    let rData = [];
    // 用户信息列表
    let users = {};
    for(let i = 0; i < data.length; i++) {
        let item = {};
        if(users[data[i]._openid]) {
            // 如果用户信息列表已有该用户信息，直接添加
            item.userInfo = users[data[i]._openid].userInfo;
            item.userID = users[data[i]._openid].userID;
        }else {
            // 如果没有，则查询后，添加到用户列表
            let uRes = await db.collection('users').doc(data[i]._openid).get();
            users[data[i]._openid] = uRes.data;
            item.userInfo = uRes.data.userInfo;
            item.userID = uRes.data.userID;
        }
        item.id = data[i]._id;
        item.goodsData = data[i].goodsData;
        item.goodsImg = data[i].goodsImg;
        item.goodsStatus = data[i].goodsStatus;
        item.goodsType = data[i].goodsType;

        rData.push(item);
    }
    return rData;
}
/**
 * 清洗商品数据，将用户openid去除，保护用户隐私
 * @param {*} data 
 */
function cleanIdlemallData(data) {
    let rData = [];
    for(let i = 0; i < data.length; i++) {
        let item = {};
        item.id = data[i]._id;
        item.goodsData = data[i].goodsData;
        item.goodsImg = data[i].goodsImg;
        item.goodsStatus = data[i].goodsStatus;
        item.goodsType = data[i].goodsType;
        rData.push(item);
    }
    return rData;
}
/* <===== 积分商城 =====> */
/**
 * 分页获取积分商品
 * @param {*} type 
 * @param {*} pageSize 
 * @param {*} pageNum 
 */
async function getIntegralList(type, pageSize, pageNum) {
    let rData = {};
    let begin = (pageNum - 1) * pageSize;
    let size = pageSize;
    const cRes = await db.collection('shoppingList').where({
        cate: _.in(type)
    }).count();
    let total = cRes.total;
    rData.total = cRes.total;
    rData.data = false;
    if(begin < total) {
        const sRes = await db.collection('shoppingList').where({
                                cate: _.in(type)
                            }).get();
        rData.data = sRes.data;
    }
    return rData;
}

/* <===== 意见反馈 =====> */
/**
 * 分页获取用户的历史反馈
 * @param {*} openId 
 * @param {*} pageSize 
 * @param {*} pageNum 
 */
async function getFeedback(openId, pageSize, pageNum) {
    let rData = {};
    let begin = (pageNum - 1) * pageSize;
    let size = pageSize;
    const cRes = await db.collection('feedback').where({
        _openid: openId
    }).count();
    let total = cRes.total;
    rData.total = cRes.total;
    rData.data = false;
    if(begin < total) {
        const fRes = await db.collection('feedback').where({
            _openid: openId
        }).skip(begin).limit(size).get();
        rData.data = fRes.data;
    }
    return rData;
}



/* <===== 一些辅助函数 =====> */
/**
 * 获取YYMM/DD格式的日期
 * @param {Date} date 
 */
function getTimeFormat1(date) {
    return (date.getFullYear() % 100) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2);
}
/**
 * 检查订单状态，将其添加入用户订单消息
 * @param {*} timeStamp 时间戳
 * @param {*} state 状态：0,1,2
 */
async function checkedState(openId, timeStamp) {
    let newMess = [];
    let idList = [];
    let orderList = await db.collection('recycle_order').where({
                            _openid: openId,
                            orderForm: {
                                state: 0
                            }
                        }).get();
    orderList = orderList.data;
    for(let i = 0; i < orderList.length; i++) {
        let timeList = getIntervalTimeStamp(orderList[i].orderForm.orderTime);
        if(timeStamp > timeList[0]) {
            let data = {};
            let date = getMidTimeStamp(timeList[0], timeList[1]);
            data.orderId = orderList[i]._id;
            data.createTime = getTimeFormat1(new Date(date));
            data.orderAmount = orderList[i].orderForm.amountStr;
            data.createTimeStamp = date;
            data.orderState = 1;
            data.isRead = false;
            idList.push(orderList[i]._id);
            newMess.push(data);
        }
    }
    let uOrder = await db.collection('recycle_order').where({
        _id: _.in(idList),
        _openid: openId,
        orderForm: {
            state: 0
        }
    }).update({
        data: {
            orderForm: {
                state: 1
            }
        }
    });
    newMess.sort((a, b) => { return a.createTimeStamp - b.createTimeStamp });
    return await pushRecycleMess(openId, newMess);
}

/**
 * 获取两个时间戳之间的某个时间
 * @param {*} befTime 
 * @param {*} afTime 
 */
function getMidTimeStamp(befTime, afTime) {
    let seed = (afTime - befTime) / 1000;
    let incT = Math.floor(Math.random() * seed);
    return befTime + incT * 1000;
}
/**
 * 获取给定时间段的两个时间戳
 * @param {*} dateStr 
 */
function getIntervalTimeStamp(dateStr) {
    let dateList = dateStr.split(' ');
    let date = dateList[0];
    let timeList = dateList[1].split('-');
    let befTime = new Date(date + ' ' + timeList[0]).getTime();
    let afTime = new Date(date + ' ' + timeList[1]).getTime();
    return [befTime, afTime];
}
/**
 * 通过传入的 userID 计算出对应的 userSig
 * @param {String} userID 
 */
function getUserSig(userID) {
    let SDKAPPID = _SDKAPPID;
    let SECRETKEY = _SECRETKEY;
    /**
     * 签名过期时间，建议不要设置的过短
     * <p>
     * 时间单位：秒
     * 默认时间：7 x 24 x 60 x 60 = 604800 = 7 天
     */
    let EXPIRETIME = 604800;
    let api = new TLSSigAPIv2.Api(SDKAPPID, SECRETKEY);
    let sig = api.genSig(userID, EXPIRETIME);
    return sig;
}

/**
 * 获取一个随机字符串作为userID
 */
function getUserID() {
    // let chars = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let chars = 'sbcdefghijklmnopqratuvwxyz';
    let userId = '';
    let stamp = new Date().getTime();
    let stampArr = String(stamp).split('').map(Number);
    for(let i = 0; i < stampArr.length; i++) {
        userId += chars[i];
        if(i % 5 === 0) {
            let index = Math.floor(Math.random() * 15) + 10;
            userId += chars[index];
        }
    }
    return userId;
}