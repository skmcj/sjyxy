/**
 * 生成一个订单号
 */
function orderCode(type = 'Y') {
    // 订单号组成(17)：年份(后2) + 月份 + 天数 + 毫秒数 + 小时数 + 分钟数 + 秒数 + 随机数(1)
    let date = new Date();
    let dateStr = (date.getFullYear() % 100) + ('0' + (date.getMonth() + 1)).slice(-2) + ('0' + date.getDate()).slice(-2);
    let ms = date.getMilliseconds();
    let time = ('0' + date.getHours()).slice(-2) + ('0' + date.getMonth()).slice(-2) + ('0' + date.getSeconds()).slice(-2);
    let rnum = Math.floor(Math.random() * 10);
    return type + dateStr + ms + time + rnum;
}

module.exports = {
    orderCode: orderCode
}